package com.discoverdst.photos.utils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.storage.db.DatabaseHelperManager;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * 14.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class HelpUtils {

    public static final String COMMON_TAG = "dstphotos_tag";
    public static final String DIALOG_TAG = "DIALOG_TAG";
    public static final String PROGRESS_DIALOG_TAG = "PROGRESS_DIALOG_TAG";
    public static final int PERMISSIONS_CAMERA_REQUEST_CODE = 100;

    //upload events
    public static final String START_UPLOAD_EVENT = "START_UPLOAD_EVENT";
    public static final String END_UPLOAD_EVENT = "END_UPLOAD_EVENT";


    public static int validateEmail(String email) {
        if (!isValidEmail(email)) {
            return R.string.sign_up_invalid_email;
        }
        return -1;
    }

    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void hideKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            Object inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                ((InputMethodManager) inputMethodManager).hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    /**
     * iOS code
     * <p>
     * let path = "properties/c"
     * .appending(String(forPhoto.property.company_id))
     * .appending("/p").appending(String(forPhoto.property_id))
     * .appending("/photos/")
     * .appending(forPhoto.name)
     * .appending("/")
     * </p>
     *
     * @param photo
     * @param dbHelperManager
     * @return
     */
    private static String getPhotoUrlPart(Photo photo, DatabaseHelperManager dbHelperManager) {
        StringBuilder builder = new StringBuilder();
        List<Property> properties = null;
        try {
            properties = dbHelperManager.getHelper().getPropertyDao().getPropertyById(photo.getPropertyId());
        } catch (SQLException e) {
            Log.e(HelpUtils.class.getSimpleName(), "getPhotoUrlPart: ", e);
        }

        if (properties != null && !properties.isEmpty()) {
            builder.append("/properties/c")
                    .append(properties.get(0).getCompanyId())
                    .append("/p").append(photo.getPropertyId())
                    .append("/photos/")
                    .append(photo.getName());
        }
        return builder.toString();
    }

    public static Uri getPhotoSourcePath(DatabaseHelperManager dbHelperManager, String baseUrl, Photo photo, Context context, PhotoType photoType) {
        Uri uri;
        if (!photo.isCached()) {
            uri = Uri.parse(baseUrl + getPhotoUrlPart(photo, dbHelperManager) + photoType.fileSuffix);
        } else {
            uri = Uri.fromFile(CameraHelper.createImageFilePath(context, photo.getName()));
        }
        return uri;
    }

    public static List<Photo> deletePhotosFiles(Context context, DatabaseHelperManager dbHelperManager, List<Photo> newPhotos) {
        List<Photo> photosToSave = null;
        try {
            photosToSave = dbHelperManager.getHelper().getPhotoDao().getPhotosByCache(true);
        } catch (SQLException e) {
            Log.e(COMMON_TAG, "deletePhotosFiles: ", e);
        }
        if (photosToSave != null) {
            Set<String> newNamesSet = new HashSet<>();
            for (Photo photo :
                    newPhotos) {
                newNamesSet.add(photo.getName());
            }

            for (Iterator<Photo> iterator = photosToSave.iterator(); iterator.hasNext(); ) {
                Photo photoToSave = iterator.next();
                if (newNamesSet.contains(photoToSave.getName())) {
                    CameraHelper.createImageFilePath(context, photoToSave.getName()).delete();
                    iterator.remove();
                }
            }
        }
        return photosToSave;
    }

    public static void deleteCachedAndSave(Context context, DatabaseHelperManager dbHelperManager, List<Photo> photos) {
        List<Photo> photosToSave = HelpUtils.deletePhotosFiles(context, dbHelperManager, photos);
        try {
            dbHelperManager.getHelper().getPhotoDao().updatePhotos(photos);
            if (photosToSave != null && !photosToSave.isEmpty()) {
                dbHelperManager.getHelper().getPhotoDao().createPhotos(photosToSave);
            }
        } catch (SQLException e) {
            Log.e(COMMON_TAG, "", e);
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void registerReceiver(Context context, BroadcastReceiver broadcastReceiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(HelpUtils.START_UPLOAD_EVENT);
        intentFilter.addAction(HelpUtils.END_UPLOAD_EVENT);
        LocalBroadcastManager.getInstance(context).registerReceiver(
                broadcastReceiver, intentFilter);
    }

    public enum PhotoType {
        PREVIEW("/240x240.jpg"), FULL("/original.jpg");

        private String fileSuffix;

        PhotoType(String fileSuffix) {
            this.fileSuffix = fileSuffix;
        }
    }

}
