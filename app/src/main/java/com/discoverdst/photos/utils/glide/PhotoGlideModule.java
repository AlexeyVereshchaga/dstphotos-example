package com.discoverdst.photos.utils.glide;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.engine.cache.ExternalCacheDiskCacheFactory;
import com.bumptech.glide.module.GlideModule;

/**
 * 24.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class PhotoGlideModule implements GlideModule {

    /** 100 MB of cache. */
    private static final int DISK_CACHE_SIZE = 100 * 1024 * 1024;

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        new ExternalCacheDiskCacheFactory(context, DiskCache.Factory.DEFAULT_DISK_CACHE_DIR, DISK_CACHE_SIZE);
    }

    @Override
    public void registerComponents(Context context, Glide glide) {

    }
}
