package com.discoverdst.photos.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.List;

import static android.view.OrientationEventListener.ORIENTATION_UNKNOWN;

/**
 * 31.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class CameraHelper {

    public static final String JPEG_EXTENSION = ".jpg";
    public static final int FILE_NAME_LENGTH = 32;

    private static final String TAG = CameraHelper.class.getSimpleName();
    private static final int PHOTO_SIZE = 720;
    private static SecureRandom secureRandom = new SecureRandom();

    /**
     * A safe way to get an instance of the Camera object.
     */
    @Nullable
    public static Camera getCameraInstance(int cameraId) {
        Camera c = null;
        try {
            c = Camera.open(cameraId); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
            Log.e(TAG, "getCameraInstance: ", e);
        }
        return c; // returns null if camera is unavailable
    }

    public static Camera.ShutterCallback SHUTTER_CALLBACK = new Camera.ShutterCallback() {
        @Override
        public void onShutter() {
            MediaActionSound sound = new MediaActionSound();
            sound.play(MediaActionSound.SHUTTER_CLICK);
        }
    };

    public static void cropAndSaveFile(final Context context, byte[] data, final WriteFileListener fileListener) {
        Glide.with(context)
                .load(data)
                .asBitmap()
                .override(PHOTO_SIZE, PHOTO_SIZE)
                .centerCrop()
                .format(DecodeFormat.PREFER_ARGB_8888)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        fileListener.onFileWrite(writeDataToFile(context, resource));
                    }
                });
    }

    private static String writeDataToFile(Context context, Bitmap bitmap) {
        File pictureFile = null;
        try {
            pictureFile = createImageFile(context);
        } catch (IOException e) {
            Log.e(TAG, "onPictureTaken: ", e);
        }
        if (pictureFile != null) {
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
            } catch (Exception e) {
                Log.e(TAG, "onPictureTaken: ", e);
            }
        }
        return pictureFile != null ? pictureFile.getName() : null;
    }

    private static File createImageFile(Context context) throws IOException {
        // Create an image file name
        String imageFileName = generateFileName(FILE_NAME_LENGTH);
        File imageFile = createImageFilePath(context, imageFileName);
//        File image = File.createTempFile(
//                imageFileName,  /* prefix */
//                JPEG_EXTENSION, /* suffix */
//                storageDir      /* directory */
//        );
        return imageFile;
    }

    public static File createImageFilePath(Context context, String imageFileName) {
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File file = new File(storageDir, imageFileName + JPEG_EXTENSION);
        storageDir.mkdirs();
        return file;
    }

    private static String generateFileName(int strLength) {
        String s = "abcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder sb = new StringBuilder(strLength);
        for (int i = 0; i < strLength; i++)
            sb.append(s.charAt(secureRandom.nextInt(s.length())));
        return sb.toString();
    }

    public interface WriteFileListener {
        void onFileWrite(String fileName);
    }

    public static boolean checkCameraAutoFocus(Camera camera) {
        List<String> supportedFocusModes = camera.getParameters().getSupportedFocusModes();
        return supportedFocusModes != null && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO);
    }

    public static void setCameraRotation(Camera camera, int cameraId, int orientation) {
        if (orientation == ORIENTATION_UNKNOWN) return;
        Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        orientation = (orientation + 45) / 90 * 90;
        int rotation = 0;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            rotation = (info.orientation - orientation + 360) % 360;
        } else {  // back-facing camera
            rotation = (info.orientation + orientation) % 360;
        }
        Camera.Parameters params = camera.getParameters();
        params.setRotation(rotation);
        camera.setParameters(params);
    }
}
