package com.discoverdst.photos.controller;

import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;

import com.discoverdst.photos.R;
import com.discoverdst.photos.ui.fragment.map.MapFragment_;
import com.discoverdst.photos.ui.fragment.photo.PhotoTabFragment_;
import com.discoverdst.photos.ui.fragment.settings.SettingsFragment_;
import com.discoverdst.photos.ui.fragment.status.StatusFragment_;

/**
 * 14.03.17.
 *
 * @author Alexey Vereshchaga
 */
public enum TabScreen {

    DST_PHOTOS(PhotoTabFragment_.class, "DST Photos", R.drawable.ic_tab_photo),
    MAP(MapFragment_.class, "Map", R.drawable.ic_tab_map),
    STATUS(StatusFragment_.class, "Status", R.drawable.ic_tab_status),
    SETTINGS(SettingsFragment_.class, "Settings", R.drawable.ic_tab_settings);

    private Class<? extends Fragment> fragmentClass;
    private String tab;
    private int tabIcon;

    TabScreen(Class<? extends Fragment> fragmentClass, String tab, @DrawableRes int tabIcon) {
        this.fragmentClass = fragmentClass;
        this.tab = tab;
        this.tabIcon = tabIcon;
    }

    public Class<? extends Fragment> getFragmentClass() {
        return fragmentClass;
    }

    public String getTab() {
        return tab;
    }

    public int getTabIcon() {
        return tabIcon;
    }
}