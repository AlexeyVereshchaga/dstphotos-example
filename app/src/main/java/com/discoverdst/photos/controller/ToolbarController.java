package com.discoverdst.photos.controller;

import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.discoverdst.photos.R;

import org.androidannotations.annotations.EBean;

/**
 * 13.03.17.
 *
 * @author Alexey Vereshchaga
 */

@EBean(scope = EBean.Scope.Singleton)
public class ToolbarController {
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private Button btnLeft;
    private ImageButton ibRight;

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
        toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar_title);
        btnLeft = (Button) toolbar.findViewById(R.id.btn_left);
        ibRight = (ImageButton) toolbar.findViewById(R.id.ib_right);
    }

    public void setTitle(String title) {
        toolbarTitle.setText(title);
    }

    public Button getBtnLeft() {
        return btnLeft;
    }

    public ImageButton getIbRight() {
        return ibRight;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
