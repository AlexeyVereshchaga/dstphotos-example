package com.discoverdst.photos.controller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.discoverdst.photos.R;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

/**
 * 13.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EBean(scope = EBean.Scope.Singleton)
public class TransitionsManager {

    public void startChildFragment(FragmentManager fragmentManager, Fragment fragment, boolean addToBackStack, Bundle bundle) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        String tag = fragment.getClass().getSimpleName();
        fragmentTransaction.replace(R.id.fragment_container, fragment, tag);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }

    public void startChildFragment(FragmentManager fragmentManager, Fragment fragment, boolean addToBackStack) {
        startChildFragment(fragmentManager, fragment, addToBackStack, null);
    }

    @UiThread
    public void popBackStackToFirst(final FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(0);
                fragmentManager.popBackStack(backStackEntry.getId(), android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
}
