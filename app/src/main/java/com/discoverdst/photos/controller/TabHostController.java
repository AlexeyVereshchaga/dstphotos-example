package com.discoverdst.photos.controller;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.discoverdst.photos.R;
import com.discoverdst.photos.storage.shared_preferences.LocalStorage_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * 14.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EBean(scope = EBean.Scope.Singleton)
public class TabHostController {
    @RootContext
    Context context;
    private FragmentTabHost tabHost;

    public void setup(FragmentTabHost tabHost, FragmentManager supportFragmentManager) {
        this.tabHost = tabHost;
        tabHost.setup(context, supportFragmentManager, android.R.id.tabcontent);
        for (TabScreen tabScreen :
                TabScreen.values()) {
            tabHost.addTab(
                    tabHost.newTabSpec(tabScreen.getTab()).setIndicator(getTabIndicator(context, tabScreen.getTab(), tabScreen.getTabIcon())),
                    tabScreen.getFragmentClass(), null);
        }
        if (TextUtils.isEmpty(LocalStorage_.getInstance_(context).getEmail())) {
            tabHost.setCurrentTab(3);
        } else {
            tabHost.setCurrentTab(0);
        }
    }

    private View getTabIndicator(Context context, String tab, int icon) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView);
        iv.setImageResource(icon);
        TextView tv = (TextView) view.findViewById(R.id.textView);
        tv.setText(tab);
        TextView tvBadge = (TextView) view.findViewById(R.id.tv_badge);
        tvBadge.setVisibility(View.GONE);
        return view;
    }

    public FragmentTabHost getTabHost() {
        return tabHost;
    }
}
