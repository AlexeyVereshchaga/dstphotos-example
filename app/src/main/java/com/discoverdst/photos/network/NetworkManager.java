package com.discoverdst.photos.network;

import android.content.Context;

import com.discoverdst.photos.BuildConfig;
import com.discoverdst.photos.model.Equipment;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.storage.shared_preferences.LocalStorage_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * 16.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EBean(scope = EBean.Scope.Singleton)
public class NetworkManager {

    @RootContext
    Context context;

    private OkHttpClient client;

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://" + Server.valueOf(LocalStorage_.getInstance_(context).getServer()).getWebServerUrl())
            .addConverterFactory(JacksonConverterFactory.create())
            .client(createNewOkHttpClient())
            .build();

    private DSApi dsApi = retrofit.create(DSApi.class);

    private OkHttpClient createNewOkHttpClient() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                String host = Server.valueOf(LocalStorage_.getInstance_(context).getServer()).getWebServerUrl();
                if (host != null) {
                    HttpUrl newUrl = request.url().newBuilder()
                            .host(host)
                            .build();
                    request = request.newBuilder()
                            .url(newUrl)
                            .build();
                }
                return chain.proceed(request);
            }
        };
        clientBuilder.addInterceptor(interceptor);

        HttpLoggingInterceptor loggingInterceptor = null;
        if (BuildConfig.DEBUG) {
            loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
        if (loggingInterceptor != null) {
            clientBuilder.addInterceptor(loggingInterceptor);
        }
        client = clientBuilder.build();
        return client;
    }

    public OkHttpClient getClient() {
        return client;
    }

    public Call<List<Property>> getProperties(BaseHandler<List<Property>> callback) {
        callback.onStart();
        Call<List<Property>> call = dsApi.getProperties(LocalStorage_.getInstance_(context).getEmail());
        call.enqueue(callback);
        return call;
    }

    public Call<List<Equipment>> getEquipment(BaseHandler<List<Equipment>> callback) {
        callback.onStart();
        Call<List<Equipment>> call = dsApi.getEquipment(LocalStorage_.getInstance_(context).getEmail());
        call.enqueue(callback);
        return call;
    }

    public Call<List<Photo>> getPhotos(BaseHandler<List<Photo>> callback) {
        callback.onStart();
        Call<List<Photo>> call = dsApi.getPhotos(LocalStorage_.getInstance_(context).getEmail());
        call.enqueue(callback);
        return call;
    }


}
