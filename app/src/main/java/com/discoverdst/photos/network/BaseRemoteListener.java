package com.discoverdst.photos.network;

import android.util.Log;

import retrofit2.Call;

public abstract class BaseRemoteListener<T> implements IRemoteListener<T> {

    private static final String TAG = BaseRemoteListener.class.getSimpleName();

    @Override
    public void onStartTask() {
    }

    @Override
    public void onSuccess(T result) {
    }

    @Override
    public void onFailure(Integer errorCode, String errorBody) {
        Log.e(TAG, "onFailure: errorBody:" + errorBody);
    }

    @Override
    public void onError(Throwable t) {
        if (t != null) {
            Log.e(TAG, "", t);
        }
    }

    @Override
    public void onFinishTask(Call call) {

    }
}
