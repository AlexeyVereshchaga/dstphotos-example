package com.discoverdst.photos.network.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.network.amazon.AmazonManager;
import com.discoverdst.photos.storage.db.DatabaseHelperManager;
import com.discoverdst.photos.utils.HelpUtils;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;

import java.sql.SQLException;
import java.util.List;

import static com.discoverdst.photos.utils.HelpUtils.COMMON_TAG;

@EService
public class UploadService extends Service {
    @Bean
    AmazonManager amazonManager;
    @Bean
    DatabaseHelperManager dbHelperManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(COMMON_TAG, "onCreate: ");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean isNetworkAvailable = HelpUtils.isNetworkAvailable(getApplicationContext());
        Log.d(COMMON_TAG, "onStartCommand: isNetworkAvailable: " + isNetworkAvailable);
        if (isNetworkAvailable) {
            uploadPhotosToAmazon();
        }
        return START_STICKY;
    }

    @Background
    void uploadPhotosToAmazon() {
        List<Photo> photos = null;
        try {
            photos = dbHelperManager.getHelper().getPhotoDao().getPhotosByUpload(false);
        } catch (SQLException e) {
            Log.e(COMMON_TAG, "uploadPhotosToAmazon: ", e);
        }
        if (photos != null && !amazonManager.isUploading) {
            amazonManager.uploadPhotosToAmazon(photos);
        }
    }
}
