package com.discoverdst.photos.network;

import com.discoverdst.photos.model.Equipment;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.model.Property;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * 15.03.17.
 *
 * @author Alexey Vereshchaga
 */
public interface DSApi {

    @GET("/propconfig/properties.json")
    Call<List<Property>> getProperties(@Query("email") String email);
    @GET("/propconfig/equipment.json")
    Call<List<Equipment>> getEquipment(@Query("email") String email);
    @GET("/propconfig/photos.json")
    Call<List<Photo>> getPhotos(@Query("email") String email);
}
