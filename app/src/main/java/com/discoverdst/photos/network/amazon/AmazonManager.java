package com.discoverdst.photos.network.amazon;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.discoverdst.photos.model.Equipment;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.network.BaseHandler;
import com.discoverdst.photos.network.BaseRemoteListener;
import com.discoverdst.photos.network.NetworkManager;
import com.discoverdst.photos.network.Server;
import com.discoverdst.photos.storage.db.DatabaseHelperManager;
import com.discoverdst.photos.storage.shared_preferences.LocalStorage;
import com.discoverdst.photos.utils.CameraHelper;
import com.discoverdst.photos.utils.HelpUtils;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import retrofit2.Call;

import static com.discoverdst.photos.utils.HelpUtils.COMMON_TAG;

/**
 * 04.04.17.
 *
 * @author Alexey Vereshchaga
 */
@EBean(scope = EBean.Scope.Singleton)
public class AmazonManager {

    @RootContext
    Context context;
    @Bean
    protected LocalStorage localStorage;
    @Bean
    DatabaseHelperManager dbHelperManager;
    @Bean
    NetworkManager networkManager;

    public boolean isUploading;
    private List<Photo> photosToUpload;
    private List<Call> callList = Collections.synchronizedList(new ArrayList<Call>());

    private AmazonS3Client mS3Client;
    private StaticCredentialsProvider mCredProvider;
    private TransferUtility mTransferUtility;

    private static String S3_ACCESS_KEY = "AKIAJ52H3AJCLMEBGBCQ";
    private static String S3_SECRET_KEY = "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";

    private StaticCredentialsProvider getCredProvider() {
        if (mCredProvider == null) {
            mCredProvider = new StaticCredentialsProvider(new BasicAWSCredentials(S3_ACCESS_KEY, S3_SECRET_KEY));
        }
        return mCredProvider;
    }

    private AmazonS3Client getS3Client() {
        if (mS3Client == null) {
            mS3Client = new AmazonS3Client(getCredProvider());
            mS3Client.setRegion(com.amazonaws.regions.Region.getRegion(Regions.US_WEST_2));
        }
        return mS3Client;
    }

    /**
     * Init TransferUtility which is constructed using the
     * given {@link #context}
     */
    private void initTransferUtility() {
        if (mTransferUtility == null) {
            mTransferUtility = new TransferUtility(getS3Client(),
                    context.getApplicationContext());
        }
    }

    public void uploadPhotosToAmazon(List<Photo> photos) {
        photosToUpload = Collections.synchronizedList(photos);
        if (!isUploading && photosToUpload != null && !photos.isEmpty()) {
            isUploading = true;

            sendBroadcastStartUpload();

            initTransferUtility();
            for (Photo photo :
                    photosToUpload) {
                uploadPhoto(photo);
            }
        }
    }

    @Background
    void uploadPhoto(final Photo photo) {
        Map<String, String> metadataMap = createMetadataMap(photo);
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setUserMetadata(metadataMap);
        objectMetadata.addUserMetadata("Content-Type", "image/jpeg");

        Log.d(COMMON_TAG, "uploadPhotosToAmazon: photoName: " + photo.getName());
        File file = CameraHelper.createImageFilePath(context, photo.getName());
        TransferObserver observer = null;
        try {
            observer = mTransferUtility.upload(
                    Server.valueOf(localStorage.getServer()).getPublicWriteBucket(),         /* The bucket to upload to */
                    "propconfig-photos/" + photo.getName() + CameraHelper.JPEG_EXTENSION,    /* The key for the uploaded object */
                    file,                                                                    /* The file where the data to upload exists */
                    objectMetadata
            );
        } catch (Exception e) {
            Log.e(COMMON_TAG, "uploadPhoto: ", e);
        }

        if (observer != null) {
            Log.d(COMMON_TAG, "uploadPhotosToAmazon: Bucket(): " + observer.getBucket());
            Log.d(COMMON_TAG, "uploadPhotosToAmazon: Key(): " + observer.getKey());
            Log.d(COMMON_TAG, "uploadPhotosToAmazon: AbsoluteFilePath(): " + observer.getAbsoluteFilePath());

            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    Log.d(COMMON_TAG, "onStateChanged: id: " + id + ", state: " + state);
                    if (state.equals(TransferState.COMPLETED)) {
                        Log.d(COMMON_TAG, "upload completed");
                        try {
                            photo.setUploaded(true);
                            dbHelperManager.getHelper().getPhotoDao().update(photo);
                            localStorage.setUploadedPhotos(localStorage.getUploadedPhotos() + 1);
                            sendBroadcastEndUpload();
                            removePhotoAndCheckList();
                        } catch (SQLException e) {
                            Log.e(COMMON_TAG, "onStateChanged: ", e);
                        }
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    Log.d(COMMON_TAG, "onProgressChanged: id: " + id + ", bytesCurrent: " + bytesCurrent + ", bytesTotal: " + bytesTotal);
                }

                @Override
                public void onError(int id, Exception ex) {
                    Log.d(COMMON_TAG, "onError: id: " + id + " ", ex);
                    removePhotoAndCheckList();
                }

                private void removePhotoAndCheckList() {
                    photosToUpload.remove(photo);
                    if (photosToUpload.isEmpty()) {
                        isUploading = false;
                        updateDatabase();
                    }
                }
            });
        }
    }

    @Background
    void updateDatabase() {
        callList.add(networkManager.getProperties(new BaseHandler<>(new BackgroundRemoteListener<List<Property>>() {
            @Override
            public void onSuccess(List<Property> result) {
                try {
                    dbHelperManager.getHelper().getPropertyDao().updateProperties(result);
                } catch (SQLException e) {
                    Log.e(COMMON_TAG, "", e);
                }
            }
        })));
        callList.add(networkManager.getPhotos(new BaseHandler<>(new BackgroundRemoteListener<List<Photo>>() {
            @Override
            public void onSuccess(List<Photo> result) {
                HelpUtils.deleteCachedAndSave(context, dbHelperManager, result);
            }
        })));
        callList.add(networkManager.getEquipment(new BaseHandler<>(new BackgroundRemoteListener<List<Equipment>>() {
            @Override
            public void onSuccess(List<Equipment> result) {
                try {
                    dbHelperManager.getHelper().getEquipmentDao().updateEquipment(result);
                } catch (SQLException e) {
                    Log.e(COMMON_TAG, "", e);
                }
            }
        })));
    }

    private void sendBroadcastStartUpload() {
        Intent intent = new Intent(HelpUtils.START_UPLOAD_EVENT);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void sendBroadcastEndUpload() {
        Intent intent = new Intent(HelpUtils.END_UPLOAD_EVENT);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @NonNull
    private Map<String, String> createMetadataMap(Photo photo) {
        String photoName = photo.getName();
        String companyId = null;
        List<Property> properties = null;
        try {
            properties = dbHelperManager.getHelper().getPropertyDao().getPropertyById(photo.getPropertyId());
        } catch (SQLException e) {
            Log.e(COMMON_TAG, "uploadPhotosToAmazon: ", e);
        }
        if (properties != null && properties.size() != 0) {
            companyId = String.valueOf(properties.get(0).getCompanyId());
        }
        String folder = photo.getFolder();
        String propertyId = String.valueOf(photo.getPropertyId());
        String lat = String.valueOf(photo.getLat());
        String lng = String.valueOf(photo.getLng());

        SimpleDateFormat sdf = new SimpleDateFormat(Photo.DATE_FORMAT, Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String takenAt = sdf.format(new Date());

        Map<String, String> metadataMap = new HashMap<>();
        metadataMap.put("company_id", companyId);//
        metadataMap.put("folder", folder);//
        metadataMap.put("property_id", propertyId);//
        metadataMap.put("name", photoName);//
        metadataMap.put("lat", lat);//
        metadataMap.put("lng", lng);//
        metadataMap.put("taken_at", takenAt);//
        Log.d(COMMON_TAG, "createMetadataMap: " + metadataMap.toString());
        return metadataMap;
    }

    private class BackgroundRemoteListener<T> extends BaseRemoteListener<T> {
        @Override
        public void onFinishTask(Call call) {
            if (callList != null) {
                callList.remove(call);
//                if (callList.isEmpty()) {
//                    sendDatabaseUpdatedBroadcast();
//                }
            }
        }
    }

//    private void sendDatabaseUpdatedBroadcast() {
//        Log.d(COMMON_TAG, "sendDatabaseUpdatedBroadcast: ");
//    }
}