package com.discoverdst.photos.network;

import retrofit2.Call;

/**
 * 16.02.16.
 *
 * @author Alexey Vereshchaga
 */
public interface IRemoteListener<T> {

    void onStartTask();

    void onSuccess(T result);

    void onFailure(Integer errorCode, String errorBody);

    void onError(Throwable t);

    void onFinishTask(Call call);
}
