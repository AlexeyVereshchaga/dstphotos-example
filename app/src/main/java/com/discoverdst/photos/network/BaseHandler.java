package com.discoverdst.photos.network;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseHandler<T> implements Callback<T> {

    private static final String TAG = BaseHandler.class.getSimpleName();

    private IRemoteListener<T> remoteListener;

    public BaseHandler(IRemoteListener<T> remoteListener) {
        this.remoteListener = remoteListener;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response != null) {
            if (response.isSuccessful()) {
                remoteListener.onSuccess(response.body());
            } else {
                Integer errorCode = response.code();
                String errorString = null;
                if (response.errorBody() != null) {
                    try {
                        errorString = response.errorBody().string();
                    } catch (Exception e) {
                        Log.e(TAG, "Error", e);
                    }
                }
                remoteListener.onFailure(errorCode, errorString);
                remoteListener.onError(null);
            }
        }
        remoteListener.onFinishTask(call);
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        remoteListener.onError(t);
        remoteListener.onFinishTask(call);
    }

    public void onStart() {
        remoteListener.onStartTask();
    }
}

