package com.discoverdst.photos.network;

/**
 * 14.03.17.
 *
 * @author Alexey Vereshchaga
 */
public enum Server {
    PROD("some_url.com", "dst-public-write", "dst-read"),
    STAGING("some_url.com", "dst-staging-public-write", "dst-staging-public-read");
    private String webServerUrl;
    private String publicWriteBucket;
    private String publicReadBucket;

    public static final String BASE_IMAGE_URL_AMAZON = "https://s3-us-west-2.amazonaws.com/";

    Server(String webServerUrl, String publicWriteBucket, String publicReadBucket) {
        this.webServerUrl = webServerUrl;
        this.publicWriteBucket = publicWriteBucket;
        this.publicReadBucket = publicReadBucket;
    }

    public String getWebServerUrl() {
        return webServerUrl;
    }

    public String getPublicWriteBucket() {
        return publicWriteBucket;
    }

    public String getPublicReadBucket() {
        return publicReadBucket;
    }
}
