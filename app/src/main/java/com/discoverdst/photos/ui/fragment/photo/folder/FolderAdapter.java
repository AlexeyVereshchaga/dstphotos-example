package com.discoverdst.photos.ui.fragment.photo.folder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Folder;
import com.discoverdst.photos.ui.fragment.AbstractRecycleViewAdapter;

import java.util.List;

/**
 * 23.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class FolderAdapter extends AbstractRecycleViewAdapter<Folder> {

    private FolderAdapter.OnItemViewClickListener clickListener;
    private Context context;

    public FolderAdapter(List<Folder> dataList, Context context) {
        super(dataList);
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_property, parent, false);
        return new FolderAdapter.FolderHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (dataList != null && !dataList.isEmpty()) {
            FolderAdapter.FolderHolder folderHolder = (FolderAdapter.FolderHolder) holder;
            Folder folder = dataList.get(position);
            Integer photoCount = folder.getPhotoCount();
            if (photoCount == null || photoCount < 1) {
                folderHolder.tvName.setText(folder.getName());
            } else {
                folderHolder.tvName.setText(createSpannableString(folder.getName(), photoCount));
            }
        }
    }

    private class FolderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName;

        public FolderHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onViewClick(getAdapterPosition());
            }
        }
    }

    private SpannableString createSpannableString(String itemString, Integer photoCount) {
        int itemNameEnd = itemString.length() + 1;
        int photoCountStringLength = String.valueOf(photoCount).length() + 2;
        SpannableString spannable = new SpannableString(itemString + " (" + photoCount + ")");
        spannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.gray_light)),
                itemNameEnd, itemNameEnd + photoCountStringLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public void setClickListener(FolderAdapter.OnItemViewClickListener clickListener) {
        this.clickListener = clickListener;
    }


    public interface OnItemViewClickListener {
        void onViewClick(int position);
    }
}
