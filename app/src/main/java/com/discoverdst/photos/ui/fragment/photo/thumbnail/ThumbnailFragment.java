package com.discoverdst.photos.ui.fragment.photo.thumbnail;

import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.ui.fragment.TabChildFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 27.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_thumbnail)
public class ThumbnailFragment extends TabChildFragment {
    public static final String FOLDER_KEY = "FOLDER_KEY";
    public static final String PROPERTY_ID_KEY = "PROPERTY_ID_KEY";
    private static final String TAG = ThumbnailFragment.class.getSimpleName();

    @FragmentArg(FOLDER_KEY)
    String folderName;
    @FragmentArg(PROPERTY_ID_KEY)
    Long propertyId;
    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;
    private ThumbnailAdapter mAdapter;


    @AfterViews
    void initRecyclerView() {
        LinearLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ThumbnailAdapter(new ArrayList<Photo>(), getActivity());
        mAdapter.setClickListener(new ThumbnailAdapter.OnItemViewClickListener() {
            @Override
            public void onViewClick(int position) {
                Photo photo = mAdapter.getItem(position);
                Fragment fragment = getParentFragment();
                if (fragment != null) {
                    transitionsManager.startChildFragment(fragment.getChildFragmentManager(), FullImageFragment_.builder()
                            .photoName(photo.getName())
                            .build(), true);
                }
            }
        });
        recyclerView.setAdapter(mAdapter);
        downloadThumbnail();
    }

    @Background
    void downloadThumbnail() {
        List<Photo> photos = null;
        try {
            photos = dbHelperManager.getHelper().getPhotoDao().getPhotosByFolder(folderName, propertyId);
        } catch (SQLException e) {
            Log.e(TAG, "downloadThumbnail: ", e);
        }
        if (photos != null) {
            Collections.sort(photos, Collections.reverseOrder());
        }
        resetItems(photos);
    }

    @UiThread
    void resetItems(List<Photo> photos) {
        mAdapter.resetItems(photos);
    }
}
