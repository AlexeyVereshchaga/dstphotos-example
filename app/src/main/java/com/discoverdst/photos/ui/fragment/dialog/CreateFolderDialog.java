package com.discoverdst.photos.ui.fragment.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.discoverdst.photos.R;
import com.discoverdst.photos.ui.activity.MainActivity_;
import com.discoverdst.photos.ui.fragment.status.CreateFolderListener;

/**
 * 07.04.17.
 *
 * @author Alexey Vereshchaga
 */
public class CreateFolderDialog extends DialogFragment {

    private CreateFolderListener createFolderListener;

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getString(R.string.create_folder_title);
        String message = getString(R.string.create_folder_message);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.dialog_create_folder_edit_text, null);
        final EditText editText = (EditText) view.findViewById(R.id.et_create_folder);


        builder.setPositiveButton(R.string.create_folder_button,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (createFolderListener != null) {
                            createFolderListener.onCreateFolder(editText.getText().toString());
                        }
                    }
                });
        builder.setNegativeButton(R.string.create_folder_cancel, null);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setView(view);
        return builder.create();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        createFolderListener = ((MainActivity_) getActivity()).getActionsWithFragments();
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                ContextCompat.getColor(getActivity(), R.color.colorSecondary));
        ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(
                ContextCompat.getColor(getActivity(), R.color.colorSecondary));
    }
}