package com.discoverdst.photos.ui.fragment.photo.camera;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.network.service.UploadService_;
import com.discoverdst.photos.ui.fragment.TabChildFragment;
import com.discoverdst.photos.ui.fragment.dialog.ProgressDialog;
import com.discoverdst.photos.utils.CameraHelper;
import com.discoverdst.photos.utils.HelpUtils;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import static com.discoverdst.photos.utils.CameraHelper.SHUTTER_CALLBACK;
import static com.discoverdst.photos.utils.HelpUtils.COMMON_TAG;

/**
 * 29.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_camera)
public class CameraFragment extends TabChildFragment implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String PROPERTY_ID_KEY = "PROPERTY_ID_KEY";
    private static final String PROPERTY_LAT_KEY = "PROPERTY_LAT_KEY";
    private static final String PROPERTY_LNG_KEY = "PROPERTY_LNG_KEY";
    private static final String EQUIPMENT_NAME_KEY = "EQUIPMENT_NAME_KEY ";

    @ViewById(R.id.camera_preview)
    FrameLayout flCameraPreview;
    @ViewById(R.id.ib_camera_switch)
    ImageButton ibCameraSwitch;
    @ViewsById({R.id.ib_camera_switch, R.id.cb_flash, R.id.ib_close, R.id.btn_capture})
    List<View> views;

    @FragmentArg(PROPERTY_ID_KEY)
    Long propertyId;
    @FragmentArg(PROPERTY_LAT_KEY)
    Double propertyLat;
    @FragmentArg(PROPERTY_LNG_KEY)
    Double propertyLng;
    @FragmentArg(EQUIPMENT_NAME_KEY)
    String equipmentName;

    private CameraPreview mCameraPreview;
    private Camera mCamera;
    private int cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private String flashMode = Camera.Parameters.FLASH_MODE_OFF;
    private OrientationEventListener orientationEventListener;
    private int mOrientation;

    private Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(final byte[] data, Camera camera) {
            CameraHelper.cropAndSaveFile(getActivity(), data, new CameraHelper.WriteFileListener() {
                @Override
                public void onFileWrite(final String fileName) {
                    if (fileName != null) {
                        Log.d(COMMON_TAG, "save Photo obj with fileName: " + fileName);

                        final PhotoSaver photoSaver = new PhotoSaver();
                        final ProgressDialog progressDialog = ProgressDialog.newInstance(getString(R.string.loader_getting_photo_location));
                        progressDialog.show(getFragmentManager(), HelpUtils.PROGRESS_DIALOG_TAG);

                        int millisInFuture = 10000;
                        final CountDownTimer countDownTimer = new CountDownTimer(millisInFuture, millisInFuture) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                Log.d(COMMON_TAG, "onFinish countdown: ");
                                Location location = locationManager.getLastLocation();
                                photoSaver.savePhotoToDB(location, fileName, progressDialog);
                            }
                        };

                        final LocationListener locationListener = new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {
                                Log.d(COMMON_TAG, "onLocationChanged: location: " + location);
                                photoSaver.savePhotoToDB(location, fileName, progressDialog);
                            }
                        };

                        photoSaver.setLocationListener(locationListener);
                        photoSaver.setTimer(countDownTimer);
                        countDownTimer.start();
                        locationManager.createLocationRequest(locationListener);
                    }
                }


            });
        }
    };

    @AfterViews
    void initViews() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.CAMERA},
                    HelpUtils.PERMISSIONS_CAMERA_REQUEST_CODE);
        } else {
            if (Camera.getNumberOfCameras() == 1) {
                ibCameraSwitch.setVisibility(View.INVISIBLE);
            }
            initCamera();
        }
        checkAndRequestLocationPermissions();
        locationManager.createLocationSettingsRequest(new ResultCallback() {
            @Override
            public void onResult(@NonNull Result result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        startResolutionForResult(status);
                        break;
                }
            }
        });
    }


    private void startUploadService() {
        Log.d(COMMON_TAG, "startUploadService: getActivity(): " + getActivity());
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), UploadService_.class);
            getActivity().startService(intent);
        }
    }

    private boolean checkAndRequestLocationPermissions() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
            return false;
        } else {
            return true;
        }
    }

    private void initCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
            mCameraPreview = null;
        }
        mCamera = CameraHelper.getCameraInstance(cameraId);
        setCameraParameters(mCamera, flashMode);
        mCameraPreview = new CameraPreview(getActivity(), mCamera, cameraId);
        flCameraPreview.removeAllViews();
        flCameraPreview.addView(mCameraPreview);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case HelpUtils.PERMISSIONS_CAMERA_REQUEST_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initViews();
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        toolbarController.getToolbar().setVisibility(View.GONE);
        tabHostController.getTabHost().getTabWidget().setVisibility(View.GONE);
        if (orientationEventListener == null) {
            orientationEventListener = new OrientationEventListener(getContext(), SensorManager.SENSOR_DELAY_NORMAL) {
                public void onOrientationChanged(int orientation) {
                    if (orientation == ORIENTATION_UNKNOWN) return;
                    mOrientation = orientation;
                }
            };
        }
        if (orientationEventListener.canDetectOrientation()) {
            orientationEventListener.enable();
        }
    }

    @Override
    public void onPause() {
        if (orientationEventListener != null) {
            orientationEventListener.disable();
            orientationEventListener = null;
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCamera.release();
        mCamera = null;
    }

    @Click(R.id.ib_close)
    void closeCameraFragment() {
        getBaseActivity().onBackPressed();
    }

    @Click(R.id.ib_camera_switch)
    void switchCamera() {
        cameraId =
                cameraId == Camera.CameraInfo.CAMERA_FACING_BACK
                        ? Camera.CameraInfo.CAMERA_FACING_FRONT
                        : Camera.CameraInfo.CAMERA_FACING_BACK;
        initCamera();
    }

    @Click(R.id.btn_capture)
    void capturePhoto() {
        if (!checkAndRequestLocationPermissions()) {
            return;
        }
        locationManager.createLocationSettingsRequest(new ResultCallback() {
            @Override
            public void onResult(@NonNull Result result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(HelpUtils.COMMON_TAG, "createLocationSettingsRequest: SUCCESS");
                        takePicture();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        startResolutionForResult(status);
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(HelpUtils.COMMON_TAG, "createLocationSettingsRequest: SETTINGS_CHANGE_UNAVAILABLE");
                        break;
                }
            }
        });
    }

    private void startResolutionForResult(Status status) {
        Log.i(HelpUtils.COMMON_TAG, "createLocationSettingsRequest: RESOLUTION_REQUIRED");
        try {
            status.startResolutionForResult(getBaseActivity(), 199);
        } catch (IntentSender.SendIntentException e) {
            Log.i(HelpUtils.COMMON_TAG, "PendingIntent unable to execute request.");
        }
    }

    private void takePicture() {
        enableViews(false);
        CameraHelper.setCameraRotation(mCamera, cameraId, mOrientation);
        if (CameraHelper.checkCameraAutoFocus(mCamera)) {
            mCamera.autoFocus(new Camera.AutoFocusCallback() {
                public void onAutoFocus(boolean success, Camera camera) {
                    Log.d(COMMON_TAG, "onAutoFocus: " + success);
                    if (success) {
                        mCamera.takePicture(SHUTTER_CALLBACK, null, mPictureCallback);
                    } else {
                        Toast.makeText(getActivity(), "Please take a proper focus", Toast.LENGTH_SHORT).show();
                        enableViews(true);
                    }
                }
            });
        } else {
            mCamera.takePicture(SHUTTER_CALLBACK, null, mPictureCallback);
        }
    }

    private void enableViews(boolean enable) {
        if (views != null) {
            for (View view :
                    views) {
                if (view != null) {
                    view.setEnabled(enable);
                }
            }
        }
    }

    @Click(R.id.cb_flash)
    void switchFlash(CheckBox cbFlash) {
        flashMode = cbFlash.isChecked()
                ? Camera.Parameters.FLASH_MODE_ON
                : Camera.Parameters.FLASH_MODE_OFF;
        setCameraParameters(mCamera, flashMode);
    }

    private void setCameraParameters(Camera camera, String flashMode) {
        Camera.Parameters parameters = camera.getParameters();
        parameters.setFlashMode(flashMode);
        camera.setParameters(parameters);
    }

    private class PhotoSaver {
        private CountDownTimer timer;
        private LocationListener locationListener;

        synchronized void savePhotoToDB(Location location, String fileName, ProgressDialog progressDialog) {
            if (timer != null) {
                timer.cancel();
            }
            if (locationListener != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(locationManager.getGoogleApiClient(), locationListener);
            }
            Double latitude;
            Double longitude;
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            } else {
                latitude = propertyLat;
                longitude = propertyLng;
            }
            savePhoto(fileName, latitude, longitude);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        private void savePhoto(String fileName, Double lat, Double lng) {
            Photo photo = new Photo(propertyId, equipmentName,
                    fileName.substring(0, CameraHelper.FILE_NAME_LENGTH),
                    new Date(), false, lat, lng, true);
            try {
                dbHelperManager.getHelper().getPhotoDao().savePhoto(photo);
            } catch (SQLException e) {
                Log.e(COMMON_TAG, "", e);
            }
            startUploadService();
            getBaseActivity().initPendingUploadsCounter();
            if (CameraFragment.this.isAdded()) {
                getBaseActivity().onBackPressed();
            }
        }

        void setTimer(CountDownTimer timer) {
            this.timer = timer;
        }

        void setLocationListener(LocationListener locationListener) {
            this.locationListener = locationListener;
        }
    }
}
