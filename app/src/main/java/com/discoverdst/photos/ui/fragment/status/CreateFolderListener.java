package com.discoverdst.photos.ui.fragment.status;

/**
 * 07.04.17.
 *
 * @author Alexey Vereshchaga
 */
public interface CreateFolderListener {

    void onCreateFolder(String folder);
}
