package com.discoverdst.photos.ui.fragment.map;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.discoverdst.photos.R;
import com.discoverdst.photos.controller.TabScreen;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.ui.fragment.TabFragment;
import com.discoverdst.photos.ui.fragment.listener.SelectionListener;
import com.discoverdst.photos.ui.fragment.photo.PhotoTabFragment_;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import java.sql.SQLException;
import java.util.List;

/**
 * 28.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_map)
public class MapFragment extends TabFragment implements GoogleMap.OnMarkerClickListener {

    private static final String TAG = MapFragment.class.getSimpleName();
    MapView mMapView;
    private GoogleMap googleMap;
    private CameraPosition cameraPosition;
    private List<Property> properties;
    private SelectionListener selectionListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, null);
        mMapView = (MapView) rootView.findViewById(R.id.map_view);
        mMapView.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mMapView.onResume();// needed to get the map to display immediately
            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                        @Override
                        public View getInfoWindow(Marker marker) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker marker) {
                            View v = getActivity().getLayoutInflater().inflate(R.layout.map_content, null);
                            TextView info = (TextView) v.findViewById(R.id.tv_content);
                            info.setText(marker.getTitle());
                            return v;
                        }
                    });
                    MapFragment.this.googleMap = googleMap;
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        googleMap.setMyLocationEnabled(true);
                    }
                    if (cameraPosition != null) {
                        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        cameraPosition = null;
                    }
                    getProperties();
                }
            });
        }
        return rootView;
    }

    @Background
    void getProperties() {
        List<Property> properties = null;
        try {
            properties = dbHelperManager.getHelper().getPropertyDao().getAllProperties();
        } catch (SQLException e) {
            Log.e(TAG, "getProperties: ", e);
        }
        setMarkers(properties);
    }

    @UiThread
    void setMarkers(List<Property> properties) {
        this.properties = properties;
        if (googleMap != null) {
            for (Property property :
                    properties) {
                if (property != null && property.getLat() != null && property.getLng() != null) {
                    LatLng latLng = new LatLng(property.getLat(), property.getLng());
                    MarkerOptions options = new MarkerOptions();
                    options.position(latLng).title(property.getName());
                    Marker marker = googleMap.addMarker(options);
                    marker.setTag(property.getId());
                }
            }
            googleMap.setOnMarkerClickListener(this);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        selectionListener = getBaseActivity().getActionsWithFragments();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMapView != null) {
            mMapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (googleMap != null) {
            cameraPosition = googleMap.getCameraPosition();
        }
        if (mMapView != null) {
            mMapView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView != null) {
            mMapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (selectionListener != null && properties != null) {
            for (Property property :
                    properties) {
                if (marker.getTag().equals(property.getId())) {
                    selectionListener.onItemSelect(property);
                    PhotoTabFragment_ photoTabFragment_ = (PhotoTabFragment_) getActivity().getSupportFragmentManager().findFragmentByTag(TabScreen.DST_PHOTOS.getTab());
                    if (photoTabFragment_ != null) {
                        photoTabFragment_.setNeedPopBackStack(true);
                    }
                }
            }
        }
        return false;
    }
}
