package com.discoverdst.photos.ui.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.discoverdst.photos.R;

/**
 * 15.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class CustomDialog extends DialogFragment {

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";

    public static CustomDialog newInstance(String title, String message) {
        CustomDialog frag = new CustomDialog();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);
        frag.setArguments(args);
        return frag;
    }

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString(TITLE);
        String message = getArguments().getString(MESSAGE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        builder.setPositiveButton(R.string.dialog_ok, null);
        builder.setTitle(title);
        builder.setMessage(message);
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                ContextCompat.getColor(getActivity(), R.color.colorSecondary));
    }
}
