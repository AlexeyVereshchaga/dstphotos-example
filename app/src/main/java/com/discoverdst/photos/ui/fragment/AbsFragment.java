package com.discoverdst.photos.ui.fragment;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.discoverdst.photos.network.NetworkManager;
import com.discoverdst.photos.controller.TabHostController;
import com.discoverdst.photos.controller.ToolbarController;
import com.discoverdst.photos.controller.TransitionsManager;
import com.discoverdst.photos.storage.db.DatabaseHelperManager;
import com.discoverdst.photos.storage.shared_preferences.LocalStorage;
import com.discoverdst.photos.ui.activity.AbsActivity;
import com.discoverdst.photos.utils.location.LocationManager;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;

/**
 * 14.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment
public class AbsFragment<T extends AbsActivity> extends Fragment {
    private T baseActivity;
    @Bean
    protected LocalStorage localStorage;
    @Bean
    protected NetworkManager networkManager;
    @Bean
    protected ToolbarController toolbarController;
    @Bean
    protected TransitionsManager transitionsManager;
    @Bean
    protected DatabaseHelperManager dbHelperManager;
    @Bean
    protected TabHostController tabHostController;
    @Bean
    protected LocationManager locationManager;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        baseActivity = (T) activity;
    }

    @Override
    public void onResume() {
        toolbarController.getToolbar().setVisibility(View.VISIBLE);
        tabHostController.getTabHost().getTabWidget().setVisibility(View.VISIBLE);
        toolbarController.setTitle("");
        toolbarController.getBtnLeft().setVisibility(View.GONE);
        toolbarController.getIbRight().setVisibility(View.GONE);
        super.onResume();
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public T getBaseActivity() {
        return baseActivity;
    }
}
