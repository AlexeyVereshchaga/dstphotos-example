package com.discoverdst.photos.ui.fragment.photo.folder;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Equipment;
import com.discoverdst.photos.model.Folder;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.ui.fragment.TabChildFragment;
import com.discoverdst.photos.ui.fragment.dialog.CreateFolderDialog;
import com.discoverdst.photos.ui.fragment.listener.SelectionListener;
import com.discoverdst.photos.utils.HelpUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.discoverdst.photos.utils.HelpUtils.COMMON_TAG;

/**
 * 23.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_select_folder)
public class SelectFolderFragment extends TabChildFragment {

    public static final String PROPERTY_ID_KEY = "PROPERTY_ID_KEY";

    @FragmentArg(PROPERTY_ID_KEY)
    Long propertyId;

    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;
    private FolderAdapter mAdapter;
    private SelectionListener selectionListener;

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        selectionListener = getBaseActivity().getActionsWithFragments();
    }

    @Override
    public void onResume() {
        super.onResume();
        toolbarController.setTitle(getString(R.string.select_equipment));
        ImageButton ibRight = toolbarController.getIbRight();
        ibRight.setVisibility(View.VISIBLE);
        ibRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CreateFolderDialog().show(getFragmentManager(), HelpUtils.DIALOG_TAG);
            }
        });
    }

    @AfterViews
    void initRecyclerView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new FolderAdapter(new ArrayList<Folder>(), getActivity());
        mAdapter.setClickListener(new FolderAdapter.OnItemViewClickListener() {
            @Override
            public void onViewClick(int position) {
                selectionListener.onItemSelect(mAdapter.getItem(position));
                getBaseActivity().onBackPressed();
            }
        });
        recyclerView.setAdapter(mAdapter);
        getFolders();
    }

    @Background
    void getFolders() {
        List<Photo> photos = null;
        List<Equipment> equipments = null;
        List<Folder> folders = new ArrayList<>();
        Map<String, Integer> namesMap = new LinkedHashMap<>();
        try {
            photos = dbHelperManager.getHelper().getPhotoDao().getPhotosByPropertyId(propertyId);
            equipments = dbHelperManager.getHelper().getEquipmentDao().getEquipmentByPropertyId(propertyId);
        } catch (SQLException e) {
            Log.e(COMMON_TAG, "getProperties: ", e);
        }

        addPhotosToFolders(photos, namesMap);
        addEquipmentToFolders(equipments, namesMap);

        for (Map.Entry<String, Integer> nameEntry :
                namesMap.entrySet()) {
            folders.add(new Folder(nameEntry.getKey(), nameEntry.getValue()));
        }
        updateFoldersList(folders);
    }

    private void addPhotosToFolders(List<Photo> photos, Map<String, Integer> namesMap) {
        if (photos != null) {
            for (Photo photo :
                    photos) {
                String nameKey = photo.getFolder();
                if (namesMap.containsKey(nameKey)) {
                    Integer oldValue = namesMap.get(nameKey);
                    namesMap.put(nameKey, oldValue != null ? ++oldValue : 1);
                } else {
                    namesMap.put(nameKey, 1);
                }
            }
        }
    }

    private void addEquipmentToFolders(List<Equipment> equipments, Map<String, Integer> namesMap) {
        if (equipments != null) {
            for (Equipment equipment :
                    equipments) {
                String nameKey = equipment.getName();
                if (!namesMap.containsKey(nameKey)) {
                    namesMap.put(nameKey, null);
                }
            }
        }
    }

    @UiThread
    void updateFoldersList(List<Folder> folders) {
        if (mAdapter != null) {
            mAdapter.resetItems(folders);
        }
    }

    @Background
    public void createFolder(String folder) {
        Equipment equipment = new Equipment(null, folder, propertyId);
        try {
            dbHelperManager.getHelper().getEquipmentDao().saveEquipment(equipment);
        } catch (SQLException e) {
            Log.e(COMMON_TAG, "createFolder: ", e);
        }
        selectCreatedFolder(new Folder(folder, 0));
    }

    @UiThread
    void selectCreatedFolder(Folder folder) {
        selectionListener.onItemSelect(folder);
        getBaseActivity().onBackPressed();
    }
}
