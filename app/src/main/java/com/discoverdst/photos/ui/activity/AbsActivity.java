package com.discoverdst.photos.ui.activity;

import android.support.v7.app.AppCompatActivity;

import com.discoverdst.photos.controller.TransitionsManager;
import com.discoverdst.photos.storage.db.DatabaseHelperManager;
import com.discoverdst.photos.storage.shared_preferences.LocalStorage;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

/**
 * 13.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EActivity
public abstract class AbsActivity extends AppCompatActivity {

    @Bean
    TransitionsManager transitionsManager;
    @Bean
    LocalStorage localStorage;
    @Bean
    public DatabaseHelperManager dbHelperManager;

    public abstract int getFragmentContainerId();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbHelperManager.closeHelpers();
    }
}
