package com.discoverdst.photos.ui.fragment;

import android.view.View;
import android.widget.Button;

import com.discoverdst.photos.ui.activity.MainActivity_;

/**
 * 20.03.17.
 *
 * @author Alexey Vereshchaga
 */
public abstract class TabChildFragment extends AbsFragment<MainActivity_> {

    @Override
    public void onResume() {
        super.onResume();
        Button btnLeft = toolbarController.getBtnLeft();
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });
    }
}
