package com.discoverdst.photos.ui.activity;

import com.discoverdst.photos.controller.TabScreen;
import com.discoverdst.photos.model.Folder;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.ui.fragment.listener.SelectionListener;
import com.discoverdst.photos.ui.fragment.photo.PhotoFragment_;
import com.discoverdst.photos.ui.fragment.photo.PhotoTabFragment_;
import com.discoverdst.photos.ui.fragment.photo.folder.SelectFolderFragment_;
import com.discoverdst.photos.ui.fragment.status.CreateFolderListener;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * 21.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EBean
class ActionsWithFragments implements SelectionListener, CreateFolderListener {

    @RootContext
    MainActivity mainActivity;

    @Override
    public <T> void onItemSelect(T itemObj) {
        if (mainActivity != null) {
            PhotoTabFragment_ photoTabFragment_ = (PhotoTabFragment_) mainActivity.getSupportFragmentManager().findFragmentByTag(TabScreen.DST_PHOTOS.getTab());
            if (photoTabFragment_ != null) {
                PhotoFragment_ photoFragment_ = (PhotoFragment_) photoTabFragment_.getChildFragmentManager().findFragmentByTag(PhotoFragment_.class.getSimpleName());
                if (photoFragment_ != null) {
                    if (itemObj==null || itemObj instanceof Property) {
                        photoFragment_.setProperty((Property) itemObj);
                    } else if (itemObj instanceof Folder) {
                        photoFragment_.setFolder((Folder) itemObj);
                    }
                }
            }
        }
    }

    @Override
    public void onCreateFolder(String folder) {
        if (mainActivity != null) {
            PhotoTabFragment_ photoTabFragment_ = (PhotoTabFragment_) mainActivity.getSupportFragmentManager().findFragmentByTag(TabScreen.DST_PHOTOS.getTab());
            if (photoTabFragment_ != null) {
                SelectFolderFragment_ selectFolderFragment_ = (SelectFolderFragment_) photoTabFragment_.getChildFragmentManager().findFragmentByTag(SelectFolderFragment_.class.getSimpleName());
                if (selectFolderFragment_ != null) {
                    selectFolderFragment_.createFolder(folder);
                }
            }
        }
    }
}
