package com.discoverdst.photos.ui.fragment.status;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.discoverdst.photos.R;
import com.discoverdst.photos.ui.fragment.TabFragment;
import com.discoverdst.photos.utils.HelpUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;

import static com.discoverdst.photos.utils.HelpUtils.COMMON_TAG;

/**
 * 06.04.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_status)
public class StatusFragment extends TabFragment {

    @ViewById(R.id.tv_photos_uploaded)
    TextView tvPhotosUploaded;
    @ViewById(R.id.tv_pending_uploads)
    TextView tvPendingUploads;

    private long pendingUploads;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case HelpUtils.START_UPLOAD_EVENT:
                case HelpUtils.END_UPLOAD_EVENT:
                    initViews();
                    break;
            }
        }
    };

    @AfterViews
    void initViews() {
        getNotUploadedPhoto();
        setUploadedPhotos();
    }

    private void setUploadedPhotos() {
        if (tvPhotosUploaded != null) {
            Resources res = getResources();
            int uploadedPhotos = localStorage.getUploadedPhotos();
            String uploadedPhotosStr = res.getQuantityString(R.plurals.photo_upload, uploadedPhotos, uploadedPhotos);
            tvPhotosUploaded.setText(uploadedPhotosStr);
        }
    }

    @NonNull
    @UiThread
    void setPendingUploads() {
        if (tvPendingUploads != null) {
            Resources res = getResources();
            String pendingUploadsStr = res.getQuantityString(R.plurals.photo_pending, (int) pendingUploads, pendingUploads);
            tvPendingUploads.setText(pendingUploadsStr);
        }
        setPendingUploadsBadge();
    }

    void setPendingUploadsBadge() {
        if (tabHostController != null && tabHostController.getTabHost() != null) {
            View view = tabHostController.getTabHost().getTabWidget().getChildTabViewAt(2);
            TextView tvBadge = (TextView) view.findViewById(R.id.tv_badge);

            if (pendingUploads != 0) {
                tvBadge.setVisibility(View.VISIBLE);
                tvBadge.setText(String.valueOf(pendingUploads));
            } else {
                tvBadge.setVisibility(View.GONE);
            }
        }
    }

    @Background
    void getNotUploadedPhoto() {
        try {
            pendingUploads = dbHelperManager.getHelper().getPhotoDao().getNotUploadedPhotosCount();
        } catch (SQLException e) {
            Log.e(COMMON_TAG, "setPendingUploads: ", e);
        }
        setPendingUploads();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HelpUtils.registerReceiver(getActivity(), mBroadcastReceiver);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
        super.onDestroy();
    }
}
