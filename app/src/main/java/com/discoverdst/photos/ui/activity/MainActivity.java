package com.discoverdst.photos.ui.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.discoverdst.photos.R;
import com.discoverdst.photos.controller.TabHostController;
import com.discoverdst.photos.controller.ToolbarController;
import com.discoverdst.photos.network.service.UploadService_;
import com.discoverdst.photos.ui.fragment.TabFragment;
import com.discoverdst.photos.utils.HelpUtils;
import com.discoverdst.photos.utils.location.LocationManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;

import static com.discoverdst.photos.utils.HelpUtils.COMMON_TAG;

@EActivity(R.layout.activity_main)
public class MainActivity extends AbsActivity {
    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @ViewById(android.R.id.tabhost)
    FragmentTabHost tabHost;
    @Bean
    ToolbarController toolbarController;
    @Bean
    TabHostController tabHostController;
    @Bean
    ActionsWithFragments actionsWithFragments;
    @Bean
    LocationManager locationManager;


    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case HelpUtils.START_UPLOAD_EVENT:
                case HelpUtils.END_UPLOAD_EVENT:
                    initPendingUploadsCounter();
                    break;
            }
        }
    };

    private long pendingUploads;

    @AfterViews
    void initControllers() {
        initToolbar();
        toolbarController.setToolbar(toolbar);
        tabHostController.setup(tabHost, getSupportFragmentManager());
        initPendingUploadsCounter();
    }

    @Background
    public void initPendingUploadsCounter() {
        try {
            pendingUploads = dbHelperManager.getHelper().getPhotoDao().getNotUploadedPhotosCount();
        } catch (SQLException e) {
            Log.e(COMMON_TAG, "initPendingUploadsCounter: ", e);
        }
        setPendingUploads();
    }

    @UiThread
    void setPendingUploads() {
        if (tabHostController != null && tabHostController.getTabHost() != null) {
            View view = tabHostController.getTabHost().getTabWidget().getChildTabViewAt(2);
            TextView tvBadge = (TextView) view.findViewById(R.id.tv_badge);

            if (pendingUploads != 0) {
                tvBadge.setVisibility(View.VISIBLE);
                tvBadge.setText(String.valueOf(pendingUploads));
            } else {
                tvBadge.setVisibility(View.GONE);
            }
        }
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        View view = getLayoutInflater().inflate(R.layout.toolbar_layout, null);
        getSupportActionBar().setCustomView(view, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startAlarmManager();
        HelpUtils.registerReceiver(this, mBroadcastReceiver);
    }

    @Override
    protected void onStart() {
        locationManager.getGoogleApiClient().connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        locationManager.getGoogleApiClient().disconnect();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
        super.onDestroy();
    }

    private void startAlarmManager() {
        Log.d(COMMON_TAG, "startAlarmManager: ");
        Intent intent = new Intent(getApplicationContext(), UploadService_.class);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        long minuteInMillis = 60 * 1000;
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + minuteInMillis, minuteInMillis, pendingIntent);
    }

    @Override
    public int getFragmentContainerId() {
        return android.R.id.tabcontent;
    }

    @Override
    public void onBackPressed() {
        Fragment tabFragment = getSupportFragmentManager().findFragmentById(android.R.id.tabcontent);
        if (tabFragment != null) {
            ((TabFragment) tabFragment).onBackPressed();
        }
    }

    public void close() {
        super.onBackPressed();
    }

    public ActionsWithFragments getActionsWithFragments() {
        return actionsWithFragments;
    }
}
