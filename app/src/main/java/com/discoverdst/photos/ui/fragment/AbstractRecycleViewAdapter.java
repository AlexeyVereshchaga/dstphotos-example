package com.discoverdst.photos.ui.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * 20.03.17.
 *
 * @author Alexey Vereshchaga
 */
public abstract class AbstractRecycleViewAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected List<T> dataList;

    public AbstractRecycleViewAdapter(List<T> dataList) {
        this.dataList = dataList;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void resetItems(@NonNull List<T> newDataSet) {
        dataList.clear();
        addItems(newDataSet);
    }

    public void addItems(@NonNull List<T> newDataSetItems) {
        dataList.addAll(newDataSetItems);
        notifyDataSetChanged();
    }

    public void addItem(T item) {
        if (!dataList.contains(item)) {
            dataList.add(item);
            notifyItemInserted(dataList.size() - 1);
        }
    }

    public void removeItem(T item) {
        int indexOfItem = dataList.indexOf(item);
        if (indexOfItem != -1) {
            this.dataList.remove(indexOfItem);
            notifyItemRemoved(indexOfItem);
        }
    }

    public T getItem(int index) {
        if (dataList != null && !dataList.isEmpty() && dataList.get(index) != null) {
            return dataList.get(index);
        } else {
            throw new IllegalArgumentException("Item with index " + index + " doesn't exist, dataList is " + dataList);
        }
    }

    public List<T> getDataList() {
        return dataList;
    }
}
