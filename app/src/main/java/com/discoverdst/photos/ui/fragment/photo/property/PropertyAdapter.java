package com.discoverdst.photos.ui.fragment.photo.property;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.storage.db.DatabaseHelperManager_;
import com.discoverdst.photos.ui.fragment.AbstractRecycleViewAdapter;

import java.sql.SQLException;
import java.util.List;

/**
 * 20.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class PropertyAdapter extends AbstractRecycleViewAdapter<Property> {
    private OnItemViewClickListener clickListener;
    private Context context;
    private List<Property> dataListCopy;

    public PropertyAdapter(List<Property> dataList, Context context) {
        super(dataList);
        dataListCopy = dataList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_property, parent, false);
        return new PropertyHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (dataList != null && !dataList.isEmpty()) {
            PropertyHolder propertyHolder = (PropertyHolder) holder;
            Property property = dataList.get(position);
            String itemString = property.getName();
            Long photoCount = null;
            try {
                photoCount = DatabaseHelperManager_.getInstance_(context).getHelper().getPhotoDao().getPhotosCountByPropertyId(property.getId());
            } catch (SQLException e) {
                Log.e(getClass().getSimpleName(), "onBindViewHolder: ", e);
            }
            Resources resources = context.getResources();
            if (photoCount == null || photoCount == 0) {
                propertyHolder.tvName.setText(itemString);
                float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, resources.getDisplayMetrics());
                propertyHolder.tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.item_name_text_size) - px);
            } else {
                propertyHolder.tvName.setText(createSpannableString(itemString, photoCount));
                propertyHolder.tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.item_name_text_size));
            }
        }
    }

    private SpannableString createSpannableString(String itemString, Long photoCount) {
        int itemNameEnd = itemString.length() + 1;
        int photoCountStringLength = String.valueOf(photoCount).length();
        SpannableString spannable = new SpannableString(itemString + " " + photoCount);
        spannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.gray_light)),
                itemNameEnd, itemNameEnd + photoCountStringLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    private class PropertyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName;

        public PropertyHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onViewClick(getAdapterPosition());
            }
        }
    }

    public void setClickListener(OnItemViewClickListener clickListener) {
        this.clickListener = clickListener;
    }


    public interface OnItemViewClickListener {
        void onViewClick(int position);
    }

    @Override
    public void resetItems(@NonNull List<Property> newDataSet) {
        dataListCopy = newDataSet;
        super.resetItems(newDataSet);
    }

    public void filter(String text, boolean autoconfig) {
        dataList.clear();
        if (text.isEmpty() && !autoconfig) {
            dataList.addAll(dataListCopy);
        } else {
            text = text.toLowerCase();
            if (!autoconfig) {
                for (Property item : dataListCopy) {
                    if (item.getName().toLowerCase().contains(text)) {
                        dataList.add(item);
                    }
                }
            } else {
                for (Property item : dataListCopy) {
                    if (item.getName().toLowerCase().contains(text)
                            && item.getAutoconfig() != null
                            && item.getAutoconfig()) {
                        dataList.add(item);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }
}
