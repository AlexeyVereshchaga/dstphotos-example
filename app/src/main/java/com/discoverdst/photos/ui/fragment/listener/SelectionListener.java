package com.discoverdst.photos.ui.fragment.listener;

/**
 * 21.03.17.
 *
 * @author Alexey Vereshchaga
 */
public interface SelectionListener {
    <T> void  onItemSelect(T itemObj);
}
