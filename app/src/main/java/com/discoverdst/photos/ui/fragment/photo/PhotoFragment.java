package com.discoverdst.photos.ui.fragment.photo;

import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Folder;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.network.Server;
import com.discoverdst.photos.ui.activity.MainActivity_;
import com.discoverdst.photos.ui.fragment.AbsFragment;
import com.discoverdst.photos.ui.fragment.dialog.CustomDialog;
import com.discoverdst.photos.ui.fragment.photo.camera.CameraFragment_;
import com.discoverdst.photos.ui.fragment.photo.folder.SelectFolderFragment_;
import com.discoverdst.photos.ui.fragment.photo.property.SelectPropertyFragment_;
import com.discoverdst.photos.ui.fragment.photo.thumbnail.ThumbnailFragment_;
import com.discoverdst.photos.utils.HelpUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;


/**
 * 17.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_photo)
public class PhotoFragment extends AbsFragment<MainActivity_> {

    private static final String TAG = PhotoFragment.class.getSimpleName();

    @ViewById(R.id.tv_property)
    TextView tvProperty;
    @ViewById(R.id.tv_folder)
    TextView tvFolder;
    @ViewById(R.id.tv_photos_count)
    TextView tvPhotosCount;
    @ViewById(R.id.iv_photo_1)
    ImageView ivPhoto1;
    @ViewById(R.id.iv_photo_2)
    ImageView ivPhoto2;
    @ViewById(R.id.tv_take_photo)
    TextView tvTakePhoto;

    private Property property;
    private Folder folder;
    private int photosCount;
    private List<Photo> photos;

    @Override
    public void onResume() {
        super.onResume();
        toolbarController.setTitle(getString(R.string.dst_photos));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @AfterViews
    void initViews() {
        getPhotoCount();
        updatePhotosCount();
        tvProperty.setText(property != null ? property.getName() : getString(R.string.photo_select_property));
        tvFolder.setText(folder != null ? folder.getName() : getString(R.string.photo_select_equipment));
        tvTakePhoto.setEnabled(folder != null);
    }

    @UiThread
    void updatePhotosCount() {
        if (isAdded()) {
            Resources res = getResources();
            String photosCountStr = res.getQuantityString(R.plurals.photo_count, photosCount, photosCount);
            tvPhotosCount.setText(photosCountStr);
            if (photosCount != 0) {
                downloadThumbnail();
            }
        }
    }

    @Background
    void getPhotoCount() {
        if (folder != null)
            try {
                getPhotosAndSort();
                photosCount = photos.size();
            } catch (Exception e) {
                Log.e(TAG, "getPhotoCount: ", e);
            }
        updatePhotosCount();
    }

    private void getPhotosAndSort() throws SQLException {
        photos = dbHelperManager.getHelper().getPhotoDao().getPhotosByFolder(folder.getName(), property.getId());
        if (photos != null) {
            Collections.sort(photos, Collections.reverseOrder());
        }
    }


    @Background
    void downloadThumbnail() {
        try {
            getPhotosAndSort();
        } catch (SQLException e) {
            Log.e(TAG, "downloadThumbnail: ", e);
        }
        if (photos != null) {
            String baseUrl = Server.BASE_IMAGE_URL_AMAZON + Server.valueOf(localStorage.getServer()).getPublicReadBucket();
            if (photos.size() >= 1) {
                Uri uri = HelpUtils.getPhotoSourcePath(dbHelperManager, baseUrl, photos.get(0), getContext(), HelpUtils.PhotoType.PREVIEW);
                setPhoto(uri, ivPhoto1);
            }
            if (photos.size() >= 2) {
                Uri uri = HelpUtils.getPhotoSourcePath(dbHelperManager, baseUrl, photos.get(1), getContext(), HelpUtils.PhotoType.PREVIEW);
                setPhoto(uri, ivPhoto2);
            }
        }
    }

    @UiThread
    void setPhoto(Uri uri, ImageView imageView) {
        if (uri != null) {
            @IdRes int resId = R.drawable.ic_placeholder_thumbnail;
            Glide.with(this)
                    .load(uri)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(resId)
                    .priority(Priority.IMMEDIATE)
                    .dontAnimate()
                    .into(imageView);
        }
    }

    @Click(R.id.ll_property)
    void clickOnSelectProperty() {
        Fragment fragment = getParentFragment();
        if (fragment != null) {
            transitionsManager.startChildFragment(fragment.getChildFragmentManager(), SelectPropertyFragment_.builder().build(), true);
        }
    }

    @Click(R.id.ll_folder)
    void clickOnSelectFolder() {
        if (property == null) {
            CustomDialog customDialog = CustomDialog.newInstance(null, getString(R.string.you_need_to_select));
            customDialog.show(getFragmentManager(), HelpUtils.DIALOG_TAG);
        } else {
            Fragment fragment = getParentFragment();
            if (fragment != null) {
                transitionsManager.startChildFragment(fragment.getChildFragmentManager(),
                        SelectFolderFragment_.builder()
                                .propertyId(property.getId())
                                .build(),
                        true);
            }
        }
    }

    @Click(R.id.tv_take_photo)
    void takePhoto() {
        Fragment fragment = getParentFragment();
        if (fragment != null && folder != null) {
            transitionsManager.startChildFragment(fragment.getChildFragmentManager(),
                    CameraFragment_.builder()
                            .equipmentName(folder.getName())
                            .propertyId(property.getId())
                            .propertyLat(property.getLat())
                            .propertyLng(property.getLng())
                            .build(),
                    true);
        }
    }

    @Click(R.id.iv_photo_1)
    void clickOnPhoto1() {
        startThumbnailFragment(1);
    }

    @Click(R.id.iv_photo_2)
    void clickOnPhoto2() {
        startThumbnailFragment(2);
    }

    private void startThumbnailFragment(int photosCount) {
        Fragment fragment = getParentFragment();
        if (fragment != null && folder != null && this.photosCount >= photosCount) {
            transitionsManager.startChildFragment(fragment.getChildFragmentManager(),
                    ThumbnailFragment_.builder()
                            .folderName(folder.getName())
                            .propertyId(property.getId())
                            .build(),
                    true);
        }
    }

    public void setProperty(Property newProperty) {
        property = newProperty;
        folder = null;
        if (tvTakePhoto != null) {
            tvTakePhoto.setEnabled(false);
        }
        photosCount = 0;
    }

    public void setFolder(Folder newFolder) {
        folder = newFolder;
        if (tvTakePhoto != null) {
            tvTakePhoto.setEnabled(folder != null);
        }
    }
}