package com.discoverdst.photos.ui.fragment.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

/**
 * 17.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class ProgressDialog extends DialogFragment {

    private static final String MESSAGE = "message";

    public static ProgressDialog newInstance(String message) {
        ProgressDialog frag = new ProgressDialog();
        Bundle args = new Bundle();
        args.putString(MESSAGE, message);
        frag.setArguments(args);
        return frag;
    }

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString(MESSAGE);
        return android.app.ProgressDialog.show(getActivity(), null, message, true);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {

        super.onDismiss(dialog);
    }
}
