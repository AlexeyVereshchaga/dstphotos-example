package com.discoverdst.photos.ui.fragment.settings;


import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Equipment;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.network.BaseHandler;
import com.discoverdst.photos.network.BaseRemoteListener;
import com.discoverdst.photos.network.Server;
import com.discoverdst.photos.ui.fragment.TabFragment;
import com.discoverdst.photos.ui.fragment.dialog.ChoiceDialog;
import com.discoverdst.photos.ui.fragment.dialog.CustomDialog;
import com.discoverdst.photos.ui.fragment.dialog.ProgressDialog;
import com.discoverdst.photos.utils.HelpUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;

/**
 * 13.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_settings)
public class SettingsFragment extends TabFragment {

    private String TAG = SettingsFragment.class.getSimpleName();
    private List<Call> calls = new ArrayList<>();
    private volatile int requestCounter;
    private ProgressDialog progressDialog;

    @ViewById(R.id.tv_current_version)
    TextView tvCurrentAppVersion;
    @ViewById(R.id.et_email)
    EditText etEmail;

    @AfterViews
    void initViews() {
        try {
            String versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            tvCurrentAppVersion.setText(String.format(Locale.US, getString(R.string.settings_current_app_version), versionName));
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(SettingsFragment.class.getSimpleName(), "initViews: ", e);
        }
        etEmail.setText(localStorage.getEmail());
    }

    @Click(R.id.tv_sync)
    void syncData() {
        if (HelpUtils.isNetworkAvailable(getContext())) {
            String email = etEmail.getText().toString();
            int invalidEmailMessageId = HelpUtils.validateEmail(email);
            if (invalidEmailMessageId != -1) {
                CustomDialog.newInstance(getString(R.string.dialog_title_email_needed),
                        getString(invalidEmailMessageId)).show(getFragmentManager(), HelpUtils.DIALOG_TAG);
                return;
            }
            localStorage.saveEmail(email);
            HelpUtils.hideKeyboard(getActivity());
            getData();
        } else {
            CustomDialog.newInstance(getString(R.string.dialog_title_error),
                    getString(R.string.dialog_message_offline)).show(getFragmentManager(), HelpUtils.DIALOG_TAG);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        progressDialog = (ProgressDialog) getFragmentManager().findFragmentByTag(HelpUtils.PROGRESS_DIALOG_TAG);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void getData() {
        progressDialog = ProgressDialog.newInstance(getString(R.string.syncing_property_data));
        progressDialog.show(getFragmentManager(), HelpUtils.PROGRESS_DIALOG_TAG);
        calls.add(networkManager.getProperties(new BaseHandler<>(new PropertiesRemoteListener<List<Property>>() {
            @Override
            public void onSuccess(List<Property> result) {
                saveProperties(result);
            }

            @Override
            public void onFinishTask(Call call) {
                calls.add(networkManager.getEquipment(new BaseHandler<>(new PropertiesRemoteListener<List<Equipment>>() {
                    @Override
                    public void onSuccess(List<Equipment> result) {
                        saveEquipment(result);
                    }
                })));
                calls.add(networkManager.getPhotos(new BaseHandler<>(new PropertiesRemoteListener<List<Photo>>() {
                    @Override
                    public void onSuccess(List<Photo> result) {
                        savePhotos(result);
                    }
                })));
                super.onFinishTask(call);
            }
        })));
    }

    @Background
    protected void saveProperties(List<Property> properties) {
        try {
            dbHelperManager.getHelper().getPropertyDao().updateProperties(properties);
        } catch (SQLException e) {
            Log.e(TAG, "updateProperties: ", e);
        }
    }

    @Background
    protected void savePhotos(List<Photo> photos) {
        HelpUtils.deleteCachedAndSave(getContext(), dbHelperManager, photos);
        downloadThumbnails(photos);
    }

    @Background
    protected void saveEquipment(List<Equipment> equipment) {
        try {
            dbHelperManager.getHelper().getEquipmentDao().updateEquipment(equipment);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    @Background
    protected void downloadThumbnails(List<Photo> photos) {
        if (getContext() != null) {
            String urlReadPart = (Server.valueOf(localStorage.getServer())).getPublicReadBucket();
            String baseUrl = Server.BASE_IMAGE_URL_AMAZON + urlReadPart;
            for (Photo photo :
                    photos) {
                Uri uri = HelpUtils.getPhotoSourcePath(dbHelperManager, baseUrl, photo, getContext(), HelpUtils.PhotoType.PREVIEW);
                Glide.with(getContext())
                        .load(uri)
                        .downloadOnly(-1, -1);
            }
        }
    }

    @Click(R.id.tv_current_version)
    void chooseServer() {
        //// TODO: 14.04.17
//        if (BuildConfig.DEBUG) {
        new ChoiceDialog().show(getFragmentManager(), HelpUtils.DIALOG_TAG);
//        }
    }

    private class PropertiesRemoteListener<T> extends BaseRemoteListener<T> {

        @Override
        public void onStartTask() {
            requestCounter++;
        }


        @Override
        public void onFinishTask(Call call) {
            requestCounter--;
            if (requestCounter == 0) {
                if (progressDialog != null && progressDialog.isAdded()) {
                    progressDialog.dismiss();
                }
            }
        }
    }
}
