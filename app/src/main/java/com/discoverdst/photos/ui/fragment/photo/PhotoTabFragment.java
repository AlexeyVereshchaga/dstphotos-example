package com.discoverdst.photos.ui.fragment.photo;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.discoverdst.photos.R;
import com.discoverdst.photos.ui.fragment.TabFragment;

import org.androidannotations.annotations.EFragment;

/**
 * 20.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_tab)
public class PhotoTabFragment extends TabFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        transitionsManager.startChildFragment(getChildFragmentManager(), PhotoFragment_.builder().build(), false);
        super.onCreate(savedInstanceState);
    }
}