package com.discoverdst.photos.ui.fragment;

import com.discoverdst.photos.ui.activity.MainActivity_;
import com.discoverdst.photos.utils.HelpUtils;

/**
 * 20.03.17.
 *
 * @author Alexey Vereshchaga
 */
public abstract class TabFragment extends AbsFragment<MainActivity_> {

    private boolean needPopBackStack;

    public void onBackPressed() {
        HelpUtils.hideKeyboard(getActivity());
        if (getChildFragmentManager().getBackStackEntryCount() > 0) {
            getChildFragmentManager().popBackStackImmediate();
        } else {
            getBaseActivity().close();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (needPopBackStack) {
            transitionsManager.popBackStackToFirst(getChildFragmentManager());
        }
        needPopBackStack = false;
    }

    public void setNeedPopBackStack(boolean needPopBackStack) {
        this.needPopBackStack = needPopBackStack;
    }
}
