package com.discoverdst.photos.ui.fragment.photo.thumbnail;

import android.net.Uri;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.network.Server;
import com.discoverdst.photos.ui.fragment.TabChildFragment;
import com.discoverdst.photos.ui.view.TouchImageView;
import com.discoverdst.photos.utils.HelpUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.List;

/**
 * 27.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_full_image)
public class FullImageFragment extends TabChildFragment {
    public static final String PHOTO_KEY = "PHOTO_KEY";
    private static final String TAG = FullImageFragment.class.getSimpleName();

    @FragmentArg(PHOTO_KEY)
    String photoName;
    @ViewById(R.id.iv_full_photo)
    TouchImageView ivFullPhoto;

    @AfterViews
    void initView() {
        getPhotoPath();
    }

    @Background
    void getPhotoPath() {
        List<Photo> photos = null;
        String baseUrl = Server.BASE_IMAGE_URL_AMAZON + Server.valueOf(localStorage.getServer()).getPublicReadBucket();
        try {
            photos = dbHelperManager.getHelper().getPhotoDao().getPhotoByName(photoName);
        } catch (SQLException e) {
            Log.e(TAG, "getPhotoUrlPart: ", e);
        }
        if (photos != null && photos.size() >= 1) {
            Photo photo = photos.get(0);
            Uri uri = HelpUtils.getPhotoSourcePath(dbHelperManager, baseUrl, photo, getActivity(), HelpUtils.PhotoType.FULL);
            setImage(uri);
        }
    }

    @UiThread
    void setImage(Uri uri) {
        try {
            int placeholderId = HelpUtils.isNetworkAvailable(getContext()) ? R.drawable.ic_placeholder_full_image : R.drawable.ic_offline;
            Glide.with(this)
                    .load(uri)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .placeholder(placeholderId)
                    .priority(Priority.IMMEDIATE)
                    .into(ivFullPhoto);
        } catch (Exception e) {
            Log.e(TAG, "setImage: ", e);
        }
    }
}
