package com.discoverdst.photos.ui.fragment.photo.thumbnail;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.network.Server;
import com.discoverdst.photos.storage.db.DatabaseHelperManager_;
import com.discoverdst.photos.storage.shared_preferences.LocalStorage_;
import com.discoverdst.photos.ui.fragment.AbstractRecycleViewAdapter;
import com.discoverdst.photos.utils.HelpUtils;

import java.util.ArrayList;

/**
 * 27.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class ThumbnailAdapter extends AbstractRecycleViewAdapter<Photo> {

    private Context context;
    private OnItemViewClickListener clickListener;

    public ThumbnailAdapter(ArrayList<Photo> photos, Context context) {
        super(photos);
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_thumbnail, parent, false);
        return new ThumbnailAdapter.ThumbnailHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (dataList != null && !dataList.isEmpty()) {
            ThumbnailAdapter.ThumbnailHolder thumbnailHolder = (ThumbnailAdapter.ThumbnailHolder) holder;
            Photo photo = dataList.get(position);
            String baseUrl = Server.BASE_IMAGE_URL_AMAZON + Server.valueOf(LocalStorage_.getInstance_(context).getServer()).getPublicReadBucket();
            Uri uri = HelpUtils.getPhotoSourcePath(DatabaseHelperManager_.getInstance_(context), baseUrl, photo, context, HelpUtils.PhotoType.PREVIEW);
            Glide.with(context)
                    .load(uri)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_placeholder_thumbnail)
                    .fitCenter()
                    .crossFade()
                    .dontAnimate()
                    .into(thumbnailHolder.imageView);
        }
    }

    private class ThumbnailHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;

        public ThumbnailHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imageView = (ImageView) itemView.findViewById(R.id.iv_photo);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onViewClick(getAdapterPosition());
            }
        }
    }

    public void setClickListener(ThumbnailAdapter.OnItemViewClickListener clickListener) {
        this.clickListener = clickListener;
    }


    public interface OnItemViewClickListener {
        void onViewClick(int position);
    }
}
