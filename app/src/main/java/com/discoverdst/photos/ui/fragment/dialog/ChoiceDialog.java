package com.discoverdst.photos.ui.fragment.dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.discoverdst.photos.R;
import com.discoverdst.photos.controller.TabScreen;
import com.discoverdst.photos.network.NetworkManager_;
import com.discoverdst.photos.network.Server;
import com.discoverdst.photos.storage.db.DatabaseHelperManager_;
import com.discoverdst.photos.storage.shared_preferences.LocalStorage_;
import com.discoverdst.photos.ui.activity.MainActivity_;
import com.discoverdst.photos.ui.fragment.listener.SelectionListener;
import com.discoverdst.photos.ui.fragment.photo.PhotoTabFragment_;

import static com.discoverdst.photos.network.Server.PROD;

/**
 * 14.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class ChoiceDialog extends DialogFragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private SelectionListener selectionListener;

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.dialog_choice, null);
        initChoiceView(view);
        builder.setView(view);
        return builder.create();
    }

    private void initChoiceView(View view) {
        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.rg_server);
        String serverStr = LocalStorage_.getInstance_(getActivity()).getServer();
        if (!TextUtils.isEmpty(serverStr)) {
            Server server = Server.valueOf(serverStr);
            switch (server) {
                case PROD:
                    ((RadioButton) view.findViewById(R.id.rb_production)).setChecked(true);
                    break;
                case STAGING:
                    ((RadioButton) view.findViewById(R.id.rb_staging)).setChecked(true);
                    break;
            }
        }
        radioGroup.setOnCheckedChangeListener(this);
        view.findViewById(R.id.btn_cancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        selectionListener = ((MainActivity_) getActivity()).getActionsWithFragments();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        Server server = null;
        switch (checkedId) {
            case R.id.rb_production:
                server = PROD;
                break;
            case R.id.rb_staging:
                server = Server.STAGING;
                break;
        }
        if (server != null) {
            LocalStorage_.getInstance_(getActivity()).saveServer(server.name());
            NetworkManager_.getInstance_(getActivity()).getClient().dispatcher().cancelAll();
            if (selectionListener != null) {
                selectionListener.onItemSelect(null);
                PhotoTabFragment_ photoTabFragment_ = (PhotoTabFragment_) getActivity().getSupportFragmentManager().findFragmentByTag(TabScreen.DST_PHOTOS.getTab());
                if (photoTabFragment_ != null) {
                    photoTabFragment_.setNeedPopBackStack(true);
                }
            }
        }
        dismiss();
    }
}
