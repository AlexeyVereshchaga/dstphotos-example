package com.discoverdst.photos.ui.fragment.photo.property;


import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.discoverdst.photos.R;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.ui.fragment.TabChildFragment;
import com.discoverdst.photos.ui.fragment.dialog.ProgressDialog;
import com.discoverdst.photos.ui.fragment.listener.SelectionListener;
import com.discoverdst.photos.utils.HelpUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 17.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EFragment(R.layout.fragment_select_property)
public class SelectPropertyFragment extends TabChildFragment {

    private static final String TAG = "SelectProperty";

    @ViewById(R.id.sv_search)
    SearchView svSearch;
    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;
    @ViewById(R.id.rg_autoconfig)
    RadioGroup rgAutoconfig;
    @ViewById(R.id.rb_autoconfig_no)
    RadioButton rbAutoconfigNo;
    @ViewById(R.id.rb_autoconfig_yes)
    RadioButton rbAutoconfigYes;

    private PropertyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SelectionListener selectionListener;
    private List<Property> properties;
    private ProgressDialog progressDialog;
    private boolean autoconfig;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        selectionListener = getBaseActivity().getActionsWithFragments();
    }

    @Override
    public void onResume() {
        super.onResume();
        toolbarController.setTitle(getString(R.string.select_property));
    }

    @AfterViews
    void initRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        properties = new ArrayList<>();
        mAdapter = new PropertyAdapter(properties, getActivity());
        mAdapter.setClickListener(new PropertyAdapter.OnItemViewClickListener() {
            @Override
            public void onViewClick(int position) {
                selectionListener.onItemSelect(mAdapter.getItem(position));
                getBaseActivity().onBackPressed();
            }
        });
        recyclerView.setAdapter(mAdapter);
        progressDialog = ProgressDialog.newInstance(null);
        progressDialog.show(getFragmentManager(), HelpUtils.PROGRESS_DIALOG_TAG);
        getProperties();
        svSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mAdapter != null) {
                    mAdapter.filter(newText, autoconfig);
                }
                return true;
            }
        });
        rbAutoconfigNo.setChecked(true);
        rgAutoconfig.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rb_autoconfig_no:
                        autoconfig = false;
                        break;
                    case R.id.rb_autoconfig_yes:
                        autoconfig = true;
                        break;
                }
                mAdapter.filter(svSearch.getQuery().toString(), autoconfig);
            }
        });
    }

    @Background
    void getProperties() {
        try {
            properties = dbHelperManager.getHelper().getPropertyDao().getAllProperties();
        } catch (SQLException e) {
            Log.e(TAG, "getProperties: ", e);
        }
        updatePropertiesList(properties);
    }

    @UiThread
    void updatePropertiesList(List<Property> properties) {
        mAdapter.resetItems(properties);
        if (progressDialog != null && progressDialog.isAdded()) {
            progressDialog.dismiss();
        }
    }

    @Click(R.id.sv_search)
    void clickSearch() {
        svSearch.setIconified(false);
    }
}