package com.discoverdst.photos.model;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * 16.03.17.
 *
 * @author Alexey Vereshchaga
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable(tableName = "photos")
public class Photo implements Comparable<Photo> {

    public static final String PROPERTY_ID_COLUMN_NAME = "property_id";
    public static final String FOLDER_COLUMN_NAME = "folder";
    public static final String NAME_COLUMN_NAME = "name";
    public static final String UPLOADED_COLUMN_NAME = "uploaded";
    public static final String CACHED_COLUMN_NAME = "cached";

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    @DatabaseField(columnName = PROPERTY_ID_COLUMN_NAME)
    @JsonProperty("property_id")
    private Long propertyId;
    @DatabaseField(columnName = FOLDER_COLUMN_NAME)
    @JsonProperty("folder")
    private String folder;
    @DatabaseField(id = true, columnName = NAME_COLUMN_NAME)
    @JsonProperty("name")
    private String name;
    @DatabaseField(dataType = DataType.DATE, format = DATE_FORMAT)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    @JsonProperty("taken_at")
    private Date takenAt;
    @DatabaseField(columnName = UPLOADED_COLUMN_NAME)
    @JsonProperty("uploaded")
    private Boolean uploaded;
    @DatabaseField
    @JsonProperty("lat")
    private Double lat;
    @DatabaseField
    @JsonProperty("lng")
    private Double lng;
    @DatabaseField(columnName = CACHED_COLUMN_NAME)
    private boolean cached;
    @JsonIgnore
    private String heading;
    @JsonIgnore
    private String alt;

    /**
     * needed by ormlite
     */
    public Photo() {
    }

    public Photo(Long propertyId, String folder, String name, Date takenAt, Boolean uploaded, Double lat, Double lng, boolean cached) {
        this.propertyId = propertyId;
        this.folder = folder;
        this.name = name;
        this.takenAt = takenAt;
        this.uploaded = uploaded;
        this.lat = lat;
        this.lng = lng;
        this.cached = cached;
    }

    @JsonProperty("property_id")
    public Long getPropertyId() {
        return propertyId;
    }

    @JsonProperty("property_id")
    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    @JsonProperty("folder")
    public String getFolder() {
        return folder;
    }

    @JsonProperty("folder")
    public void setFolder(String folder) {
        this.folder = folder;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("taken_at")
    public Date getTakenAt() {
        return takenAt;
    }

    @JsonProperty("taken_at")
    public void setTakenAt(Date takenAt) {
        this.takenAt = takenAt;
    }

    @JsonProperty("uploaded")
    public Boolean getUploaded() {
        return uploaded;
    }

    @JsonProperty("uploaded")
    public void setUploaded(Boolean uploaded) {
        this.uploaded = uploaded;
    }

    @JsonProperty("lat")
    public Double getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(Double lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public Double getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public boolean isCached() {
        return cached;
    }

    public void setCached(boolean cached) {
        this.cached = cached;
    }

    @Override
    public int compareTo(@NonNull Photo o) {
        if (getTakenAt() == null || o.getTakenAt() == null)
            return 0;
        return getTakenAt().compareTo(o.getTakenAt());
    }
}
