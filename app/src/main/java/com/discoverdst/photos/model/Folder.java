package com.discoverdst.photos.model;

/**
 * 23.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class Folder {

    private String name;
    private Integer photoCount;

    public Folder(String name, Integer photoCount) {
        this.name = name;
        this.photoCount = photoCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(Integer photoCount) {
        this.photoCount = photoCount;
    }
}
