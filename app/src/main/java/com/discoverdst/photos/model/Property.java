package com.discoverdst.photos.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * 16.03.17.
 *
 * @author Alexey Vereshchaga
 */
@DatabaseTable(tableName = "properties")
public class Property {

    public static final String ID_FIELD_NAME = "id";
    @DatabaseField(id = true, dataType = DataType.LONG_OBJ, columnName = ID_FIELD_NAME)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("name")
    private String name;
    @DatabaseField
    @JsonProperty("lat")
    private Double lat;
    @DatabaseField
    @JsonProperty("lng")
    private Double lng;
    @DatabaseField
    @JsonProperty("autoconfig")
    private Boolean autoconfig;
    @DatabaseField
    @JsonProperty("company_id")
    private Long companyId;

    /**
     * needed by ormlite
     */
    public Property() {
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("lat")
    public Double getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(Double lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public Double getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(Double lng) {
        this.lng = lng;
    }

    @JsonProperty("autoconfig")
    public Boolean getAutoconfig() {
        return autoconfig;
    }

    @JsonProperty("autoconfig")
    public void setAutoconfig(Boolean autoconfig) {
        this.autoconfig = autoconfig;
    }

    @JsonProperty("company_id")
    public Long getCompanyId() {
        return companyId;
    }

    @JsonProperty("company_id")
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}
