package com.discoverdst.photos.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * 16.03.17.
 *
 * @author Alexey Vereshchaga
 */
@DatabaseTable(tableName = "equipment")
public class Equipment {

    public static final String PROPERTY_ID_FIELD_NAME = "property_id";

    @DatabaseField(id = true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("name")
    private String name;
    @DatabaseField(columnName = PROPERTY_ID_FIELD_NAME)
    @JsonProperty("property_id")
    private Long propertyId;

    /**
     * needed by ormlite
     */
    public Equipment() {
    }

    public Equipment(Long id, String name, Long propertyId) {
        this.id = id;
        this.name = name;
        this.propertyId = propertyId;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("property_id")
    public Long getPropertyId() {
        return propertyId;
    }

    @JsonProperty("property_id")
    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }
}