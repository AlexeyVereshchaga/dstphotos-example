package com.discoverdst.photos.storage.db.dao;

import com.discoverdst.photos.model.Photo;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * 22.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class PhotoDao extends BaseDaoImpl<Photo, Void> {
    public PhotoDao(ConnectionSource connectionSource, Class<Photo> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Photo> getAllPhotos() throws SQLException {
        return queryForAll();
    }

    public long getPhotosCountByPropertyId(Long propertyId) throws SQLException {
        QueryBuilder<Photo, Void> queryBuilder = queryBuilder();
        queryBuilder.setCountOf(true);
        queryBuilder.where().eq(Photo.PROPERTY_ID_COLUMN_NAME, propertyId);
        PreparedQuery<Photo> preparedQuery = queryBuilder.prepare();
        return countOf(preparedQuery);
    }

    public List<Photo> getPhotosByPropertyId(Long propertyId) throws SQLException {
        QueryBuilder<Photo, Void> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Photo.PROPERTY_ID_COLUMN_NAME, propertyId);
        PreparedQuery<Photo> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public List<Photo> getPhotosByFolder(String folder, Long propertyId) throws SQLException {
        String escapedFolder = folder.replace("'", "''");
        QueryBuilder<Photo, Void> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Photo.FOLDER_COLUMN_NAME, escapedFolder).and().eq(Photo.PROPERTY_ID_COLUMN_NAME, propertyId);
        PreparedQuery<Photo> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public List<Photo> getPhotoByName(String name) throws SQLException {
        QueryBuilder<Photo, Void> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Photo.NAME_COLUMN_NAME, name);
        PreparedQuery<Photo> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public List<Photo> getPhotosByUpload(boolean uploaded) throws SQLException {
        QueryBuilder<Photo, Void> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Photo.UPLOADED_COLUMN_NAME, uploaded);
        PreparedQuery<Photo> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public int deleteByName(String name) throws SQLException {
        DeleteBuilder<Photo, Void> deleteBuilder = deleteBuilder();
        deleteBuilder.where().eq(Photo.NAME_COLUMN_NAME, name);
        return deleteBuilder.delete();
    }

    public void updatePhotos(List<Photo> photos) throws SQLException {
        TableUtils.clearTable(getConnectionSource(), Photo.class);
        create(photos);
    }

    public int createPhotos(List<Photo> photos) throws SQLException {
        return create(photos);
    }

    public void updatePhoto(Photo photo) throws SQLException {
        update(photo);
    }

    public void savePhoto(Photo photo) throws SQLException {
        create(photo);
    }

    public long getNotUploadedPhotosCount() throws SQLException {
        QueryBuilder<Photo, Void> queryBuilder = queryBuilder();
        queryBuilder.setCountOf(true);
        queryBuilder.where().eq(Photo.UPLOADED_COLUMN_NAME, false);
        PreparedQuery<Photo> preparedQuery = queryBuilder.prepare();
        return countOf(preparedQuery);
    }

    public List<Photo> getPhotosByCache(boolean cached) throws SQLException {
        QueryBuilder<Photo, Void> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Photo.CACHED_COLUMN_NAME, cached);
        PreparedQuery<Photo> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }
}
