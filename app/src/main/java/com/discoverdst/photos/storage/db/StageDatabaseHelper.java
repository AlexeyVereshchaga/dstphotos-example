package com.discoverdst.photos.storage.db;

import android.content.Context;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 19.04.17.
 *
 * @author Alexey Vereshchaga
 */
public class StageDatabaseHelper extends DatabaseHelper {

    private static final String DATABASE_NAME_STAGE = "discovery_sound_stage.db";

    // we do this so there is only one helper
    private static StageDatabaseHelper helper = null;

    public StageDatabaseHelper(Context context) {
        super(context, DATABASE_NAME_STAGE);
    }

    /**
     * Get the helper, possibly constructing it if necessary. For each call to this method, there should be 1 and only 1
     * call to {@link #close()}.
     */
    public static synchronized StageDatabaseHelper getHelper(Context context) {
        if (helper == null) {
            helper = new StageDatabaseHelper(context);
        }
        return helper;
    }

    @Override
    public void close() {
        super.close();
        helper = null;
    }
}
