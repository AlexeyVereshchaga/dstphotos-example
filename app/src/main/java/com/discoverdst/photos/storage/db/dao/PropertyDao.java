package com.discoverdst.photos.storage.db.dao;

import com.discoverdst.photos.model.Property;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * 22.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class PropertyDao extends BaseDaoImpl<Property, Long> {

    public PropertyDao(ConnectionSource connectionSource, Class<Property> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Property> getAllProperties() throws SQLException {
        return queryForAll();
    }

    public void updateProperties(List<Property> properties) throws SQLException {
        TableUtils.clearTable(getConnectionSource(), Property.class);
        create(properties);
    }

    public List<Property> getPropertyById(Long propertyId) throws SQLException {
        QueryBuilder<Property, Long> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Property.ID_FIELD_NAME, propertyId);
        PreparedQuery<Property> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }
}
