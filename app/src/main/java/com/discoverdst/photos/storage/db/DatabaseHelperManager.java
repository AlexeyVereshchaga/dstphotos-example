package com.discoverdst.photos.storage.db;

import android.content.Context;

import com.discoverdst.photos.network.Server;
import com.discoverdst.photos.storage.shared_preferences.LocalStorage;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * 22.03.17.
 *
 * @author Alexey Vereshchaga
 */
@EBean(scope = EBean.Scope.Singleton)
public class DatabaseHelperManager {

    @RootContext
    Context context;
    @Bean
    LocalStorage localStorage;

    private ProdDatabaseHelper prodDatabaseHelper;
    private StageDatabaseHelper stageDatabaseHelper;

    public DatabaseHelper getHelper() {
        Server server = Server.valueOf(localStorage.getServer());
        switch (server) {
            case PROD:
                return getProdDatabaseHelper(context);
            case STAGING:
                return getStageDatabaseHelper(context);
        }
        return null;
    }

    /**
     * You'll need this in your class to get the helper from the manager once per class.
     */
    private ProdDatabaseHelper getProdDatabaseHelper(Context context) {
        if (prodDatabaseHelper == null) {
            prodDatabaseHelper = ProdDatabaseHelper.getHelper(context);
        }
        return prodDatabaseHelper;
    }

    /**
     * You'll need this in your class to get the helper from the manager once per class.
     */
    private StageDatabaseHelper getStageDatabaseHelper(Context context) {
        if (stageDatabaseHelper == null) {
            stageDatabaseHelper = StageDatabaseHelper.getHelper(context);
        }
        return stageDatabaseHelper;
    }

    public void closeHelpers() {
        if (prodDatabaseHelper != null) {
            prodDatabaseHelper.close();
            prodDatabaseHelper = null;
        }
        if (stageDatabaseHelper != null) {
            stageDatabaseHelper.close();
            stageDatabaseHelper = null;
        }
    }
}