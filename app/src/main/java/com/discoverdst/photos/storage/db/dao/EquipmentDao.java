package com.discoverdst.photos.storage.db.dao;

import com.discoverdst.photos.model.Equipment;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * 22.03.17.
 *
 * @author Alexey Vereshchaga
 */
public class EquipmentDao extends BaseDaoImpl<Equipment, Long> {
    public EquipmentDao(ConnectionSource connectionSource, Class<Equipment> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Equipment> getAllEquipment() throws SQLException {
        return queryForAll();
    }

    public void updateEquipment(List<Equipment> equipmentList) throws SQLException {
        TableUtils.clearTable(getConnectionSource(), Equipment.class);
        create(equipmentList);
    }

    public List<Equipment> getEquipmentByPropertyId(Long propertyId) throws SQLException {
        QueryBuilder<Equipment, Long> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Equipment.PROPERTY_ID_FIELD_NAME, propertyId);
        PreparedQuery<Equipment> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public void saveEquipment(Equipment equipment) throws SQLException {
        create(equipment);
    }
}
