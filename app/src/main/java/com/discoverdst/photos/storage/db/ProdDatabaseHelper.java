package com.discoverdst.photos.storage.db;

import android.content.Context;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 19.04.17.
 *
 * @author Alexey Vereshchaga
 */
public class ProdDatabaseHelper extends DatabaseHelper {

    private static final String DATABASE_NAME_PROD = "discovery_sound_prod.db";

    // we do this so there is only one helper
    private static ProdDatabaseHelper helper = null;

    public ProdDatabaseHelper(Context context) {
        super(context, DATABASE_NAME_PROD);
    }

    /**
     * Get the helper, possibly constructing it if necessary. For each call to this method, there should be 1 and only 1
     * call to {@link #close()}.
     */
    public static synchronized ProdDatabaseHelper getHelper(Context context) {
        if (helper == null) {
            helper = new ProdDatabaseHelper(context);
        }
        return helper;
    }

    @Override
    public void close() {
        super.close();
        helper = null;
    }
}
