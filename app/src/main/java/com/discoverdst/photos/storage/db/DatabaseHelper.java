package com.discoverdst.photos.storage.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.discoverdst.photos.model.Equipment;
import com.discoverdst.photos.model.Photo;
import com.discoverdst.photos.model.Property;
import com.discoverdst.photos.storage.db.dao.EquipmentDao;
import com.discoverdst.photos.storage.db.dao.PhotoDao;
import com.discoverdst.photos.storage.db.dao.PropertyDao;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * 22.03.17.
 *
 * @author Alexey Vereshchaga
 */
public abstract class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();

    // /data/data/DSTPhotos/discovery_sound_.db
    private static final int DATABASE_VERSION = 1;

    private PropertyDao propertyDao;
    private PhotoDao photoDao;
    private EquipmentDao equipmentDao;


    public DatabaseHelper(Context context, String databaseName) {
        super(context, databaseName, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Property.class);
            TableUtils.createTable(connectionSource, Photo.class);
            TableUtils.createTable(connectionSource, Equipment.class);
        } catch (SQLException e) {
            Log.e(TAG, "error creating DB " + database);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Property.class, true);
            TableUtils.dropTable(connectionSource, Photo.class, true);
            TableUtils.dropTable(connectionSource, Equipment.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "error upgrading db " + database + "from ver " + oldVersion);
            throw new RuntimeException(e);
        }
    }

    public PropertyDao getPropertyDao() throws SQLException {
        if (propertyDao == null) {
            propertyDao = new PropertyDao(getConnectionSource(), Property.class);
        }
        return propertyDao;
    }

    public PhotoDao getPhotoDao() throws SQLException {
        if (photoDao == null) {
            photoDao = new PhotoDao(getConnectionSource(), Photo.class);
        }
        return photoDao;
    }

    public EquipmentDao getEquipmentDao() throws SQLException {
        if (equipmentDao == null) {
            equipmentDao = new EquipmentDao(getConnectionSource(), Equipment.class);
        }
        return equipmentDao;
    }

    @Override
    public void close() {
        super.close();
        propertyDao = null;
        photoDao = null;
        equipmentDao = null;
    }
}
