package com.discoverdst.photos.storage.shared_preferences;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * 14.03.17.
 *
 * @author Alexey Vereshchaga
 */

@EBean(scope = EBean.Scope.Singleton)
public class LocalStorage {

    @Pref
    MyPrefs_ prefs;

    public void saveEmail(String email) {
        prefs.email().put(email);
    }

    public String getEmail() {
        return prefs.email().get();
    }

    public void saveServer(String server) {
        prefs.server().put(server);
    }

    public String getServer() {
        return prefs.server().get();
    }

    public void setUploadedPhotos(int uploadedPhotos){
        prefs.uploadedPhotos().put(uploadedPhotos);
    }

    public int getUploadedPhotos(){
        return prefs.uploadedPhotos().get();
    }
}
