package com.discoverdst.photos.storage.shared_preferences;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * 14.03.17.
 *
 * @author Alexey Vereshchaga
 */
@SharedPref(value = SharedPref.Scope.APPLICATION_DEFAULT)
public interface MyPrefs {

    String email();

    @DefaultString("PROD")
    String server();

    @DefaultInt(0)
    int uploadedPhotos();
}
